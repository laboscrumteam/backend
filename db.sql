-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 17 Cze 2019, 17:11
-- Wersja serwera: 10.1.38-MariaDB
-- Wersja PHP: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `castlewars`
--
CREATE DATABASE IF NOT EXISTS `castlewars` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `castlewars`;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `army`
--

DROP TABLE IF EXISTS `army`;
CREATE TABLE `army` (
  `id` int(11) NOT NULL,
  `village_id` int(11) NOT NULL,
  `spear_fighter` int(11) NOT NULL,
  `swordsman` int(11) NOT NULL,
  `archer` int(11) NOT NULL,
  `cavalary` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Tabela Truncate przed wstawieniem `army`
--

TRUNCATE TABLE `army`;
--
-- Zrzut danych tabeli `army`
--

INSERT INTO `army` (`id`, `village_id`, `spear_fighter`, `swordsman`, `archer`, `cavalary`) VALUES
(1, 21, 100, 25, 0, 0),
(2, 22, 177, 37, 0, 0),
(3, 23, 0, 0, 0, 0),
(4, 24, 0, 0, 0, 0),
(5, 25, 0, 0, 0, 0),
(6, 26, 0, 0, 0, 0),
(7, 27, 0, 0, 0, 0),
(8, 28, 0, 0, 0, 0),
(9, 29, 0, 0, 0, 0),
(10, 30, 0, 0, 0, 0),
(11, 31, 0, 0, 0, 0),
(12, 32, 0, 0, 0, 0),
(13, 33, 0, 0, 0, 0),
(14, 34, 0, 0, 0, 0),
(15, 35, 0, 0, 0, 0),
(16, 36, 0, 0, 0, 0),
(17, 37, 0, 0, 0, 0),
(18, 38, 0, 0, 0, 0),
(19, 39, 0, 0, 0, 0),
(20, 40, 0, 0, 0, 0),
(21, 41, 0, 0, 0, 0),
(22, 42, 0, 0, 0, 0),
(23, 43, 0, 0, 0, 0),
(24, 44, 0, 0, 0, 0),
(25, 45, 0, 0, 0, 0),
(26, 46, 0, 0, 0, 0),
(27, 47, 0, 0, 0, 0),
(28, 48, 0, 0, 0, 0),
(29, 49, 0, 0, 0, 0),
(30, 50, 0, 0, 0, 0),
(31, 51, 0, 0, 0, 0),
(32, 52, 0, 0, 0, 0),
(33, 53, 0, 0, 0, 0),
(34, 54, 0, 0, 0, 0),
(35, 55, 0, 0, 0, 0),
(36, 56, 0, 0, 0, 0),
(37, 57, 0, 0, 0, 0),
(38, 58, 0, 0, 0, 0),
(39, 59, 0, 0, 0, 0),
(40, 60, 0, 0, 0, 0),
(41, 61, 0, 0, 0, 0),
(42, 62, 0, 0, 0, 0),
(43, 63, 0, 0, 0, 0),
(44, 64, 0, 0, 0, 0),
(45, 65, 0, 0, 0, 0),
(46, 66, 0, 0, 0, 0),
(47, 67, 0, 0, 0, 0),
(48, 68, 0, 0, 0, 0),
(49, 69, 0, 0, 0, 0),
(50, 70, 0, 0, 0, 0),
(51, 71, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `authority`
--

DROP TABLE IF EXISTS `authority`;
CREATE TABLE `authority` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `code` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Tabela Truncate przed wstawieniem `authority`
--

TRUNCATE TABLE `authority`;
--
-- Zrzut danych tabeli `authority`
--

INSERT INTO `authority` (`id`, `name`, `code`) VALUES
(1, 'Test Authority', 'test'),
(2, 'EditAccount', 'editaccount'),
(3, 'Play', 'play'),
(4, 'User', 'user');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `buildings`
--

DROP TABLE IF EXISTS `buildings`;
CREATE TABLE `buildings` (
  `building_id` int(11) NOT NULL,
  `building_name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Tabela Truncate przed wstawieniem `buildings`
--

TRUNCATE TABLE `buildings`;
--
-- Zrzut danych tabeli `buildings`
--

INSERT INTO `buildings` (`building_id`, `building_name`) VALUES
(1, 'headquadrates'),
(2, 'barracks'),
(3, 'timbercamp'),
(4, 'claypit'),
(5, 'ironmine'),
(6, 'centralsquare'),
(7, 'warehouse'),
(8, 'wall'),
(9, 'farm');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `buildings_queue`
--

DROP TABLE IF EXISTS `buildings_queue`;
CREATE TABLE `buildings_queue` (
  `id` int(11) NOT NULL,
  `village_id` int(11) NOT NULL,
  `building_id` varchar(20) NOT NULL,
  `start_timestamp` bigint(30) NOT NULL,
  `end_timestamp` bigint(30) NOT NULL,
  `received` tinyint(1) NOT NULL,
  `next_lvl` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Tabela Truncate przed wstawieniem `buildings_queue`
--

TRUNCATE TABLE `buildings_queue`;
--
-- Zrzut danych tabeli `buildings_queue`
--

INSERT INTO `buildings_queue` (`id`, `village_id`, `building_id`, `start_timestamp`, `end_timestamp`, `received`, `next_lvl`) VALUES
(107, 21, 'HEADQUARTERS', 1560108742743, 1560109582743, 1, 2),
(108, 21, 'HEADQUARTERS', 1560109582743, 1560110782743, 1, 3),
(109, 21, 'CENTRAL_SQUARE', 1560108841792, 1560109141792, 1, 1),
(110, 21, 'BARRACKS', 1560113312206, 1560113600206, 1, 5),
(111, 22, 'CENTRAL_SQUARE', 1560379063076, 1560379363076, 1, 1),
(112, 22, 'HEADQUARTERS', 1560379081569, 1560379921569, 1, 2),
(113, 22, 'HEADQUARTERS', 1560391944444, 1560393144444, 1, 3),
(114, 22, 'BARRACKS', 1560391958887, 1560392488087, 1, 2),
(115, 22, 'HEADQUARTERS', 1560396009217, 1560397622017, 1, 4),
(116, 22, 'BARRACKS', 1560684017473, 1560684863473, 1, 3),
(117, 22, 'BARRACKS', 1560688355037, 1560689652237, 1, 4),
(118, 22, 'HEADQUARTERS', 1560688462639, 1560690605839, 1, 5),
(119, 22, 'BARRACKS', 1560691701651, 1560693523251, 1, 5),
(120, 22, 'HEADQUARTERS', 1560716054653, 1560718814653, 1, 6),
(121, 22, 'WAREHOUSE', 1560716065208, 1560716782808, 1, 2),
(122, 28, 'HEADQUARTERS', 1560726148373, 1560726988373, 1, 2),
(123, 28, 'HEADQUARTERS', 1560726988373, 1560728188373, 1, 3),
(124, 28, 'HEADQUARTERS', 1560728188373, 1560729868373, 1, 4),
(125, 28, 'CENTRAL_SQUARE', 1560726625549, 1560726925549, 1, 1),
(126, 28, 'BARRACKS', 1560726641298, 1560726941298, 1, 1),
(127, 28, 'TIMBER_CAMP', 1560727589869, 1560727942669, 1, 2),
(128, 28, 'IRON_MINE', 1560727591659, 1560727944459, 1, 2),
(129, 28, 'CLAY_PIT', 1560727592586, 1560727945386, 1, 2),
(130, 28, 'WAREHOUSE', 1560742474764, 1560743207964, 0, 2),
(131, 32, 'HEADQUARTERS', 1560780206534, 1560781046534, 0, 2),
(132, 32, 'CENTRAL_SQUARE', 1560780325464, 1560780625464, 0, 1),
(133, 34, 'CENTRAL_SQUARE', 1560782329407, 1560782629407, 0, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `oauth_client_details`
--

DROP TABLE IF EXISTS `oauth_client_details`;
CREATE TABLE `oauth_client_details` (
  `client_id` varchar(256) NOT NULL,
  `resource_ids` varchar(256) DEFAULT NULL,
  `client_secret` varchar(256) DEFAULT NULL,
  `scope` varchar(256) DEFAULT NULL,
  `authorized_grant_types` varchar(256) DEFAULT NULL,
  `web_server_redirect_uri` varchar(256) DEFAULT NULL,
  `authorities` varchar(256) DEFAULT NULL,
  `access_token_validity` int(11) DEFAULT NULL,
  `refresh_token_validity` int(11) DEFAULT NULL,
  `additional_information` varchar(4096) DEFAULT NULL,
  `autoapprove` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Tabela Truncate przed wstawieniem `oauth_client_details`
--

TRUNCATE TABLE `oauth_client_details`;
--
-- Zrzut danych tabeli `oauth_client_details`
--

INSERT INTO `oauth_client_details` (`client_id`, `resource_ids`, `client_secret`, `scope`, `authorized_grant_types`, `web_server_redirect_uri`, `authorities`, `access_token_validity`, `refresh_token_validity`, `additional_information`, `autoapprove`) VALUES
('WEBSITE_CLIENT', 'OAUTH_RESOURCE', '$2a$10$NtwTrpZ63z3D183lAsYwTud.d/CtrqhGGdgU586NPKwH0qbpHTJdW', 'OAUTH_WEBSITE', 'password,refresh_token', '', NULL, 1800, 86400, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `recruitment_queue`
--

DROP TABLE IF EXISTS `recruitment_queue`;
CREATE TABLE `recruitment_queue` (
  `id` int(11) NOT NULL,
  `village_id` int(11) NOT NULL,
  `start_timestamp` bigint(30) NOT NULL,
  `start_recruit` tinyint(1) NOT NULL DEFAULT '0',
  `end_recruit` tinyint(1) NOT NULL DEFAULT '0',
  `recruit_value` int(11) NOT NULL,
  `recruit_now` int(11) NOT NULL,
  `unit_id` varchar(25) NOT NULL,
  `last_update` bigint(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Tabela Truncate przed wstawieniem `recruitment_queue`
--

TRUNCATE TABLE `recruitment_queue`;
--
-- Zrzut danych tabeli `recruitment_queue`
--

INSERT INTO `recruitment_queue` (`id`, `village_id`, `start_timestamp`, `start_recruit`, `end_recruit`, `recruit_value`, `recruit_now`, `unit_id`, `last_update`) VALUES
(33, 22, 1560395928702, 1, 1, 2, 2, 'SPEAR_FIGHTER', 1560396268202),
(34, 22, 1560396268202, 1, 1, 2, 2, 'SPEAR_FIGHTER', 1560396607702),
(35, 22, 1560396607702, 1, 1, 2, 2, 'SPEAR_FIGHTER', 1560396947202),
(36, 22, 1560396947202, 1, 1, 4, 4, 'SPEAR_FIGHTER', 1560397626202),
(37, 22, 1560399083410, 1, 1, 1, 1, 'SPEAR_FIGHTER', 1560399253160),
(38, 22, 1560399253160, 1, 1, 5, 5, 'SPEAR_FIGHTER', 1560400101910),
(39, 22, 1560400101910, 1, 1, 12, 12, 'SPEAR_FIGHTER', 1560402138910),
(40, 22, 1560684103101, 1, 1, 3, 3, 'SPEAR_FIGHTER', 1560684612351),
(41, 22, 1560684743637, 1, 1, 10, 10, 'SPEAR_FIGHTER', 1560686441137),
(42, 22, 1560686441137, 1, 1, 1, 1, 'SPEAR_FIGHTER', 1560686610887),
(43, 22, 1560688606156, 1, 1, 4, 4, 'SPEAR_FIGHTER', 1560689264156),
(44, 22, 1560689356074, 1, 1, 10, 10, 'SPEAR_FIGHTER', 1560690985324),
(45, 22, 1560690985324, 1, 1, 10, 10, 'SPEAR_FIGHTER', 1560692577824),
(46, 22, 1560692577824, 1, 1, 10, 10, 'SPEAR_FIGHTER', 1560694170324),
(47, 22, 1560694170324, 1, 1, 30, 30, 'SPEAR_FIGHTER', 1560698947824),
(48, 22, 1560701207537, 1, 1, 30, 30, 'SPEAR_FIGHTER', 1560705838037),
(49, 22, 1560705838037, 1, 1, 10, 10, 'SWORDSMAN', 1560707862037),
(50, 22, 1560707862037, 1, 1, 10, 10, 'SWORDSMAN', 1560709886037),
(51, 22, 1560709886037, 1, 1, 1, 1, 'SWORDSMAN', 1560710088437),
(52, 22, 1560710088437, 1, 1, 1, 1, 'SPEAR_FIGHTER', 1560710242437),
(53, 22, 1560710242437, 1, 1, 1, 1, 'SPEAR_FIGHTER', 1560710396437),
(54, 22, 1560710396437, 1, 1, 1, 1, 'SPEAR_FIGHTER', 1560710550437),
(55, 22, 1560710550437, 1, 1, 1, 1, 'SPEAR_FIGHTER', 1560710704437),
(56, 22, 1560710704437, 1, 1, 1, 1, 'SPEAR_FIGHTER', 1560710858437),
(57, 22, 1560710858437, 1, 1, 1, 1, 'SPEAR_FIGHTER', 1560711012437),
(58, 22, 1560711077866, 1, 1, 5, 5, 'SWORDSMAN', 1560712089866),
(59, 22, 1560712089866, 1, 1, 5, 5, 'SWORDSMAN', 1560713101866),
(60, 22, 1560713101866, 1, 1, 6, 6, 'SWORDSMAN', 1560714316266),
(61, 22, 1560714316266, 1, 1, 6, 6, 'SPEAR_FIGHTER', 1560715240266),
(62, 22, 1560715240266, 1, 1, 6, 6, 'SPEAR_FIGHTER', 1560716164266),
(63, 22, 1560716164266, 1, 1, 2, 2, 'SPEAR_FIGHTER', 1560716472266);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `role`
--

DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Tabela Truncate przed wstawieniem `role`
--

TRUNCATE TABLE `role`;
--
-- Zrzut danych tabeli `role`
--

INSERT INTO `role` (`id`, `name`, `description`) VALUES
(1, 'Admin', 'Admin'),
(2, 'User', 'User');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `role_authority`
--

DROP TABLE IF EXISTS `role_authority`;
CREATE TABLE `role_authority` (
  `role_id` int(11) NOT NULL,
  `authority_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Tabela Truncate przed wstawieniem `role_authority`
--

TRUNCATE TABLE `role_authority`;
--
-- Zrzut danych tabeli `role_authority`
--

INSERT INTO `role_authority` (`role_id`, `authority_id`) VALUES
(1, 1),
(2, 2),
(2, 3),
(2, 4);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `status` varchar(10) NOT NULL,
  `failed_login_attempt_count` int(2) NOT NULL,
  `last_failed_login_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Tabela Truncate przed wstawieniem `user`
--

TRUNCATE TABLE `user`;
--
-- Zrzut danych tabeli `user`
--

INSERT INTO `user` (`id`, `name`, `username`, `password`, `status`, `failed_login_attempt_count`, `last_failed_login_date`) VALUES
(1, 'Admin', 'admin', '$2a$10$5IZ9f4x6xS3UnRix.57Y8.dNieo4oP4w0ZZr5S94ZUp.Tb70QwKtS', 'A', 0, NULL),
(31, 'user15', 'user15@user.com', '$2a$10$sj.nswnQxXPC3hWX0R78lee5UCbSVg1Rgjn9ecuf07BssZ1opY9Z6', 'A', 0, NULL),
(32, 'user10', 'user@user.com', '$2a$10$SH7MDqpp4hXp/J5HQLes4.3fCBspaga74mikFMCbiDle.U06zUGq2', 'A', 0, NULL),
(33, 'user11', 'user11@user.com', '$2a$10$yLhe5D5REmX3BHx2gsRb2O6JqucQ08K9O.0xx5jqeKytOGwBwIi2G', 'A', 0, NULL),
(34, 'user12', 'user12@user.com', '$2a$10$mLDQgr2NEyIS6X4ZeAp0dODJptZJUBt9Tkgcjb1./BHT39jcwN1yC', 'A', 0, NULL),
(35, 'user13', 'user13@user.com', '$2a$10$7ehSJdmIUSWL5estgVHOQ.LU574vgeyOQiQVQomQ30xB7qneKZPd.', 'A', 0, NULL),
(36, 'user14', 'user14@user.com', '$2a$10$bXO5ReWOW1/7mPp54hTv6.AiiQBDpZL6GeGbg/bBS5Xhb3N6LcEpK', 'A', 0, NULL),
(37, 'user16', 'user1@user.com', '$2a$10$GsrqHeTbRzJ7sCpbOa.0/e11no9CQvS5IghkeZO6m/3RIKqyb5be.', 'A', 0, NULL),
(38, 'user17', 'user17@user.com', '$2a$10$l3QVS8OZw8u4ddQJddsZEOpKWiP.k/yinIjufnta/mWEmlOqyCNbW', 'A', 0, NULL),
(39, 'user18', 'user18@user.com', '$2a$10$D1XELUgwkCxKE/VGHLZSOuGtDGcCcDhpW2v7ip7MeSds1oDdt69Wq', 'A', 0, NULL),
(40, 'user19', 'user19@user.com', '$2a$10$TMugoonjy1/lE2eWj75nJOT30pMq.DzIhNYBvXzzKKRh.wL2EUyGW', 'A', 0, NULL),
(41, 'user20', 'user2@user.com', '$2a$10$E70oIkj6Du8ai7AahCRlO.Ll24rhsMErQN3qOUtNfFjUshkE2JLGS', 'A', 0, NULL),
(42, 'user21', 'user21@user.com', '$2a$10$ChOtjnlsW3YKPwV4Pa1ESO6V.JABxnaXjposzIecIoewnhBZpDoba', 'A', 0, NULL),
(43, 'user22', 'user22@user.com', '$2a$10$dBUIshEJgOUl3MC5lMP6vusjIdRZ2euI5X7HJyS9FGFoRrHgxIwiO', 'A', 0, NULL),
(44, 'user23', 'user23@user.com', '$2a$10$uK215xPFb5F8IAzsoPeXZ.NpcpPRLl6eA2ib.GTvPqmUOSVC/iq7y', 'A', 0, NULL),
(45, 'user24', 'user24@user.com', '$2a$10$mt8d515lohzhpyiE1EgzbebJA.1KmHGDPAYp5kF45LVF0Etinr36q', 'A', 0, NULL),
(46, 'user25', 'user25@user.com', '$2a$10$y6B0KWpkbkXD93CKZpS3D.wNS1OY78jsvs7kYgqS18pWAg3rLo4t2', 'A', 0, NULL),
(47, 'user26', 'user26@user.com', '$2a$10$dVgLx3bEvdT6I.WtpgDPdedj0VHA7iZ6a8D99G/cRZ7x8zROm3CRO', 'A', 0, NULL),
(48, 'user27', 'user27@user.com', '$2a$10$gTe5g.Kpz9JZDirJjFVh6.xq.ODdzM518SK3YPwB.TL8ZYKacnbUi', 'A', 0, NULL),
(49, 'user28', 'user28@user.com', '$2a$10$M5x71ZL7zvBIqtgTq3pOwO577z.QodqJkqvLIweKfwFliJLjJ2Eo6', 'A', 0, NULL),
(50, 'user29', 'user29@user.com', '$2a$10$n8gw4S0.cnyQoGZ1VBKxReouGNKi6swxijFtrMwIgrVuhLLStX0Rm', 'A', 0, NULL),
(51, 'user30', 'user30@user.com', '$2a$10$yivwRikHPhsZF1asj.XPneV.b4xsxA2QFuhEmakgYfW6A2zomW1qC', 'A', 0, NULL),
(52, 'user31', 'user31@user.com', '$2a$10$yd4iCOjqEgQbhital0jLKOyaok05YnTphmvUKyQxfqF40Hd4kead2', 'A', 0, NULL),
(53, 'user32', 'user32@user.com', '$2a$10$GbpzsuWq/FmoWEfcWiEmqeW/ZDhhwWApJXgXvIc3CsAPdCQTlzVzq', 'A', 0, NULL),
(54, 'user33', 'user33@user.com', '$2a$10$TBuIdi880/nvakiu0j4Atu2RoSD1gTKZDY65KxWjsnyAQTfk4/Qm6', 'A', 0, NULL),
(55, 'user34', 'user34@user.com', '$2a$10$4NTPyjZ2YS0ZfebibDjCnui1bfHKwRLkAmj6RWqvzOOww47vc1jGW', 'A', 0, NULL),
(56, 'user35', 'user35@user.com', '$2a$10$VGWUgf9mOvRocpVoSyshDe5cEsNOSJfiMcJpUJidYjqyepUMylbcC', 'A', 0, NULL),
(57, 'OnePunchMan', 'user36@user.com', '$2a$10$RAGjwC2xpn5qZUxCZWmvre0Hq.xqv.dsD3wDkILu11bN74.5DqxY.', 'A', 0, NULL),
(58, 'Jizuke', 'user37@user.com', '$2a$10$P9VsOcFTV/svD2nAb0lZNOFbjdKZuQlW89sR5.KTKRtnyL3JGkPxO', 'A', 0, NULL),
(59, 'Jankos', 'user38@user.com', '$2a$10$VbAV0sDo125dfKTI/Pt1DujPOXMjyKsQDhFoSKN7Lx8YES4kX7mY2', 'A', 0, NULL),
(60, 'Wunder', 'user39@user.com', '$2a$10$rEd/33ohUYp4ClLnJ9pBEuyqcpd3QX3rqMla49luxwwdnn62BYE8O', 'A', 0, NULL),
(61, 'Atilla', 'user40@user.com', '$2a$10$TLH.TnzEamTeYIwzxcuSBOdaQL8hhLs5I1WSSgFeuw.jcvbaKM03C', 'A', 0, NULL),
(62, 'WorldHero', 'user41@user.com', '$2a$10$mY0v/X3RANqg1QUXazISw.RRQeZh4F1H2Ib5IcWI2g8gcYEcnRnxW', 'A', 0, NULL),
(63, 'Champion', 'user42@user.com', '$2a$10$tg7uf2KzayIPyuEMeECjReZmquJgU.5USHPyTf63xeXElSNIsDD0O', 'A', 0, NULL),
(64, 'Aleksandra', 'user44@user.com', '$2a$10$2Hf8vFhYE32Xkh3nC7/78.XUrKKGUbRoAI2Nj83/YNoFBRCsuW2UK', 'A', 0, NULL),
(65, 'Bartlomiej', 'user45@user.com', '$2a$10$2NnQBEbt6sMYVaCSpKhGseX6EOAMJa/VUKVQn0/v57GB.stFsmhiC', 'A', 0, NULL),
(66, 'Evelyn', 'user46@user.com', '$2a$10$uLWYlfbC8ws1CB7KPDlK3.22s4aE7nsbuGn6e3ZmTitzxAXIkTrvu', 'A', 0, NULL),
(67, 'WarLadie', 'user47@user.com', '$2a$10$5l7K3H41hNb0EGpsPQ0q..56v17mVlI1t1OFXwSD4YzOgKA5WA6Zy', 'A', 0, NULL),
(68, 'Wancheda', 'user48@user.com', '$2a$10$/e7rstZhLt2gCYIwf4sWxeB94cmllYrc8uAZy8K8eUwr7iaGoGIa6', 'A', 0, NULL),
(69, 'ClarkeGriffin', 'user49@user.com', '$2a$10$iN1FSLafjG2YqmavUAAqG.CJaNor0rJs4EuxkrzAV5ytTHpgvWd3y', 'A', 0, NULL),
(70, 'Octavia', 'user50@user.com', '$2a$10$zjm.yP9s.UtyV792TQvJieLoWB06dstcl8Y5u2ZPtxZ5o5YMMXWNG', 'A', 0, NULL),
(71, 'Raven', 'user51@user.com', '$2a$10$pKyUQiGPiKG7C95Hdj0onuTvDRi9Bsrp7t0zhHvht/HbJHigZpdpu', 'A', 0, NULL),
(72, 'cloudRunner3000', 'user52@user.com', '$2a$10$Dr278Vna5Cj0J9efWEUHAuKWB0dU.riZ2Wob1hz51sZf2W3.lplGS', 'A', 0, NULL),
(73, 'Bellamy', 'user53@user.com', '$2a$10$eY35bhNayr4TOSiD3skgteL2eTGnJnb.h649biXKqg9RE9Uf2YRkO', 'A', 0, NULL),
(74, 'netflix', 'user54@user.com', '$2a$10$7G65RXiRGPQ3fumGeBFHfOhzi1O8EC.64giUYYP4sG.RNg/mcSN02', 'A', 0, NULL),
(75, 'barbian', 'user55@user.com', '$2a$10$O7.CE2YtBM8x2LPDE2BLRe8nZkGUwuLG19iXfrtODfWCCVOp6Mkey', 'A', 0, NULL),
(76, 'MatkaZiemia', 'user56@user.com', '$2a$10$4sDjHbJuqHGrXrnXazUq4ubygN9F0sPKdsaLw5ldXgzhYFSVua4Bm', 'A', 0, NULL),
(77, 'DredDevil', 'user57@user.com', '$2a$10$32LskhectE0oD/pGzzK8B.sphvZ3ZdeH1IZr.iXw4178/ynbBpZtC', 'A', 0, NULL),
(78, 'Nervarien', 'user58@user.com', '$2a$10$vS/SaWFPdBmJRrb6Nc6rxeiXYm0uWGnbA8/2yzHtZiKBlkgTJIZiW', 'A', 0, NULL),
(79, 'jasper', 'user59@user.com', '$2a$10$/Ck4ccgqUL3w2mW5NyivMOrHaf.donBbQlWdQLKHatzfd0F17jUvS', 'A', 0, NULL),
(80, 'echo00', 'user60@user.com', '$2a$10$SS5GofqBIqoKW98tg57Dkux1zqxMqLr24WBy3azmwizW./szw5WNO', 'A', 0, NULL),
(81, 'czekolada', 'user61@user.com', '$2a$10$hnQc7LIN6znLzwR2Gj/QVeHTlP/3Yip7ZkkZhe2LTSZ.5iLQYMJ82', 'A', 0, NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `user_role`
--

DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role` (
  `role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Tabela Truncate przed wstawieniem `user_role`
--

TRUNCATE TABLE `user_role`;
--
-- Zrzut danych tabeli `user_role`
--

INSERT INTO `user_role` (`role_id`, `user_id`) VALUES
(2, 31),
(2, 32),
(2, 33),
(2, 34),
(2, 35),
(2, 36),
(2, 37),
(2, 38),
(2, 39),
(2, 40),
(2, 41),
(2, 42),
(2, 43),
(2, 44),
(2, 45),
(2, 46),
(2, 47),
(2, 48),
(2, 49),
(2, 50),
(2, 51),
(2, 52),
(2, 53),
(2, 54),
(2, 55),
(2, 56),
(2, 57),
(2, 58),
(2, 59),
(2, 60),
(2, 61),
(2, 62),
(2, 63),
(2, 64),
(2, 65),
(2, 66),
(2, 67),
(2, 68),
(2, 69),
(2, 70),
(2, 71),
(2, 72),
(2, 73),
(2, 74),
(2, 75),
(2, 76),
(2, 77),
(2, 78),
(2, 79),
(2, 80),
(2, 81);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `village`
--

DROP TABLE IF EXISTS `village`;
CREATE TABLE `village` (
  `id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `headquarters` int(2) NOT NULL DEFAULT '1',
  `central_square` int(2) NOT NULL DEFAULT '1',
  `barracks` int(2) NOT NULL DEFAULT '0',
  `timber_camp` int(2) NOT NULL DEFAULT '1',
  `clay_pit` int(2) NOT NULL DEFAULT '1',
  `iron_mine` int(2) NOT NULL DEFAULT '1',
  `warehouse` int(2) NOT NULL DEFAULT '1',
  `wall` int(2) NOT NULL DEFAULT '0',
  `farm` int(2) NOT NULL DEFAULT '1',
  `points` int(11) NOT NULL,
  `location_x` int(11) NOT NULL,
  `location_y` int(11) NOT NULL,
  `last_timestamp` bigint(30) NOT NULL,
  `wood` int(11) NOT NULL DEFAULT '1000',
  `clay` int(11) NOT NULL DEFAULT '1000',
  `iron` int(11) NOT NULL DEFAULT '1000',
  `loyalty` int(2) NOT NULL,
  `population` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Tabela Truncate przed wstawieniem `village`
--

TRUNCATE TABLE `village`;
--
-- Zrzut danych tabeli `village`
--

INSERT INTO `village` (`id`, `owner_id`, `name`, `headquarters`, `central_square`, `barracks`, `timber_camp`, `clay_pit`, `iron_mine`, `warehouse`, `wall`, `farm`, `points`, `location_x`, `location_y`, `last_timestamp`, `wood`, `clay`, `iron`, `loyalty`, `population`) VALUES
(21, 31, 'Wioska user15', 3, 1, 5, 1, 1, 1, 20, 0, 1, 118, 0, 0, 1560373243237, 48326, 52726, 55004, 100, 180),
(22, 32, 'Wioska user10', 6, 1, 5, 25, 25, 25, 2, 0, 20, 295, 0, 0, 1560730766358, 2012, 2012, 2012, 100, 373),
(23, 33, 'Wioska user11', 1, 0, 0, 1, 1, 1, 1, 0, 1, 40, 51, 59, 1560723051146, 1029, 1029, 1029, 100, 75),
(24, 34, 'Wioska user12', 1, 0, 0, 1, 1, 1, 1, 0, 1, 40, 50, 52, 1560721387769, 1000, 1000, 1000, 100, 75),
(25, 35, 'Wioska user13', 1, 0, 0, 1, 1, 1, 1, 0, 1, 40, 57, 58, 1560721393896, 1000, 1000, 1000, 100, 75),
(26, 36, 'Wioska user14', 1, 0, 0, 1, 1, 1, 1, 0, 1, 40, 53, 52, 1560721401780, 1000, 1000, 1000, 100, 75),
(27, 37, 'Wioska user16', 1, 0, 0, 1, 1, 1, 1, 0, 1, 40, 52, 55, 1560721417883, 1000, 1000, 1000, 100, 75),
(28, 38, 'Wioska user17', 4, 1, 1, 2, 2, 2, 1, 0, 1, 159, 52, 56, 1560782390716, 786, 772, 1070, 100, 124),
(29, 39, 'Wioska user18', 1, 0, 0, 1, 1, 1, 1, 0, 1, 40, 58, 58, 1560721436461, 1000, 1000, 1000, 100, 75),
(30, 40, 'Wioska user19', 1, 0, 0, 1, 1, 1, 1, 0, 1, 40, 52, 54, 1560721441907, 1000, 1000, 1000, 100, 75),
(31, 41, 'Wioska user20', 1, 0, 0, 1, 1, 1, 1, 0, 1, 40, 55, 59, 1560721448184, 1000, 1000, 1000, 100, 75),
(32, 42, 'Wioska user21', 1, 0, 0, 1, 1, 1, 1, 0, 1, 40, 51, 53, 1560780431474, 1227, 1280, 1327, 100, 87),
(33, 43, 'Wioska user22', 1, 0, 0, 1, 1, 1, 1, 0, 1, 40, 55, 53, 1560721635951, 1000, 1000, 1000, 100, 75),
(34, 44, 'Wioska user23', 1, 0, 0, 1, 1, 1, 1, 0, 1, 40, 52, 51, 1560782300242, 1300, 1400, 1400, 100, 85),
(35, 45, 'Wioska user24', 1, 0, 0, 1, 1, 1, 1, 0, 1, 40, 56, 53, 1560721647030, 1000, 1000, 1000, 100, 75),
(36, 46, 'Wioska user25', 1, 0, 0, 1, 1, 1, 1, 0, 1, 40, 58, 52, 1560721651052, 1000, 1000, 1000, 100, 75),
(37, 47, 'Wioska user26', 1, 0, 0, 1, 1, 1, 1, 0, 1, 40, 51, 54, 1560721676299, 1000, 1000, 1000, 100, 75),
(38, 48, 'Wioska user27', 1, 0, 0, 1, 1, 1, 1, 0, 1, 40, 59, 56, 1560721681309, 1000, 1000, 1000, 100, 75),
(39, 49, 'Wioska user28', 1, 0, 0, 1, 1, 1, 1, 0, 1, 40, 54, 54, 1560721686423, 1000, 1000, 1000, 100, 75),
(40, 50, 'Wioska user29', 1, 0, 0, 1, 1, 1, 1, 0, 1, 40, 53, 58, 1560721696701, 1000, 1000, 1000, 100, 75),
(41, 51, 'Wioska user30', 1, 0, 0, 1, 1, 1, 1, 0, 1, 40, 58, 57, 1560721704854, 1000, 1000, 1000, 100, 75),
(42, 52, 'Wioska user31', 1, 0, 0, 1, 1, 1, 1, 0, 1, 40, 53, 56, 1560721744279, 1000, 1000, 1000, 100, 75),
(43, 53, 'Wioska user32', 1, 0, 0, 1, 1, 1, 1, 0, 1, 40, 57, 60, 1560721977406, 1000, 1000, 1000, 100, 75),
(44, 54, 'Wioska user33', 1, 0, 0, 1, 1, 1, 1, 0, 1, 40, 51, 58, 1560782856435, 1000, 1000, 1000, 100, 75),
(45, 55, 'Wioska user34', 1, 0, 0, 1, 1, 1, 1, 0, 1, 40, 58, 51, 1560782876459, 1000, 1000, 1000, 100, 75),
(46, 56, 'Wioska user35', 1, 0, 0, 1, 1, 1, 1, 0, 1, 40, 53, 55, 1560782892231, 1000, 1000, 1000, 100, 75),
(47, 57, 'Wioska OnePunchMan', 1, 0, 0, 1, 1, 1, 1, 0, 1, 40, 55, 52, 1560782913320, 1000, 1000, 1000, 100, 75),
(48, 58, 'Wioska Jizuke', 1, 0, 0, 1, 1, 1, 1, 0, 1, 40, 55, 54, 1560782933718, 1000, 1000, 1000, 100, 75),
(49, 59, 'Wioska Jankos', 1, 0, 0, 1, 1, 1, 1, 0, 1, 40, 55, 51, 1560782947486, 1000, 1000, 1000, 100, 75),
(50, 60, 'Wioska Wunder', 1, 0, 0, 1, 1, 1, 1, 0, 1, 40, 51, 52, 1560782966464, 1000, 1000, 1000, 100, 75),
(51, 61, 'Wioska Atilla', 1, 0, 0, 1, 1, 1, 1, 0, 1, 40, 54, 59, 1560782984504, 1000, 1000, 1000, 100, 75),
(52, 62, 'Wioska WorldHero', 1, 0, 0, 1, 1, 1, 1, 0, 1, 40, 56, 50, 1560783111496, 1000, 1000, 1000, 100, 75),
(53, 63, 'Wioska Champion', 1, 0, 0, 1, 1, 1, 1, 0, 1, 40, 51, 55, 1560783123984, 1000, 1000, 1000, 100, 75),
(54, 64, 'Wioska Aleksandra', 1, 0, 0, 1, 1, 1, 1, 0, 1, 40, 55, 56, 1560783410689, 1000, 1000, 1000, 100, 75),
(55, 65, 'Wioska Bartlomiej', 1, 0, 0, 1, 1, 1, 1, 0, 1, 40, 51, 51, 1560783434424, 1000, 1000, 1000, 100, 75),
(56, 66, 'Wioska Evelyn', 1, 0, 0, 1, 1, 1, 1, 0, 1, 40, 59, 57, 1560783446924, 1000, 1000, 1000, 100, 75),
(57, 67, 'Wioska WarLadie', 1, 0, 0, 1, 1, 1, 1, 0, 1, 40, 52, 53, 1560783470250, 1000, 1000, 1000, 100, 75),
(58, 68, 'Wioska Wancheda', 1, 0, 0, 1, 1, 1, 1, 0, 1, 40, 57, 51, 1560783550685, 1000, 1000, 1000, 100, 75),
(59, 69, 'Wioska ClarkeGriffin', 1, 0, 0, 1, 1, 1, 1, 0, 1, 40, 50, 63, 1560783579721, 1000, 1000, 1000, 100, 75),
(60, 70, 'Wioska Octavia', 1, 0, 0, 1, 1, 1, 1, 0, 1, 40, 58, 62, 1560783603459, 1000, 1000, 1000, 100, 75),
(61, 71, 'Wioska Raven', 1, 0, 0, 1, 1, 1, 1, 0, 1, 40, 51, 66, 1560783616239, 1000, 1000, 1000, 100, 75),
(62, 72, 'Wioska cloudRunner3000', 1, 0, 0, 1, 1, 1, 1, 0, 1, 40, 57, 67, 1560783650808, 1000, 1000, 1000, 100, 75),
(63, 73, 'Wioska Bellamy', 1, 0, 0, 1, 1, 1, 1, 0, 1, 40, 56, 61, 1560783667803, 1000, 1000, 1000, 100, 75),
(64, 74, 'Wioska netflix', 1, 0, 0, 1, 1, 1, 1, 0, 1, 40, 50, 66, 1560783708843, 1000, 1000, 1000, 100, 75),
(65, 75, 'Wioska barbian', 1, 0, 0, 1, 1, 1, 1, 0, 1, 40, 50, 60, 1560783722139, 1000, 1000, 1000, 100, 75),
(66, 76, 'Wioska MatkaZiemia', 1, 0, 0, 1, 1, 1, 1, 0, 1, 40, 56, 62, 1560783872516, 1000, 1000, 1000, 100, 75),
(67, 77, 'Wioska DredDevil', 1, 0, 0, 1, 1, 1, 1, 0, 1, 40, 51, 62, 1560783886993, 1000, 1000, 1000, 100, 75),
(68, 78, 'Wioska Nervarien', 1, 0, 0, 1, 1, 1, 1, 0, 1, 40, 59, 67, 1560783900293, 1000, 1000, 1000, 100, 75),
(69, 79, 'Wioska jasper', 1, 0, 0, 1, 1, 1, 1, 0, 1, 40, 56, 69, 1560784103904, 1000, 1000, 1000, 100, 75),
(70, 80, 'Wioska echo00', 1, 0, 0, 1, 1, 1, 1, 0, 1, 40, 56, 63, 1560784124270, 1000, 1000, 1000, 100, 75),
(71, 81, 'Wioska czekolada', 1, 0, 0, 1, 1, 1, 1, 0, 1, 40, 55, 61, 1560784173932, 1000, 1000, 1000, 100, 75);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `army`
--
ALTER TABLE `army`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `authority`
--
ALTER TABLE `authority`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`);

--
-- Indeksy dla tabeli `buildings`
--
ALTER TABLE `buildings`
  ADD PRIMARY KEY (`building_id`);

--
-- Indeksy dla tabeli `buildings_queue`
--
ALTER TABLE `buildings_queue`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `oauth_client_details`
--
ALTER TABLE `oauth_client_details`
  ADD PRIMARY KEY (`client_id`);

--
-- Indeksy dla tabeli `recruitment_queue`
--
ALTER TABLE `recruitment_queue`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`);

--
-- Indeksy dla tabeli `role_authority`
--
ALTER TABLE `role_authority`
  ADD PRIMARY KEY (`role_id`,`authority_id`),
  ADD KEY `fk_role_has_authority_authority1_idx` (`authority_id`),
  ADD KEY `fk_role_has_authority_role_idx` (`role_id`);

--
-- Indeksy dla tabeli `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indeksy dla tabeli `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`role_id`,`user_id`),
  ADD KEY `fk_role_has_user_user1_idx` (`user_id`),
  ADD KEY `fk_role_has_user_role1_idx` (`role_id`);

--
-- Indeksy dla tabeli `village`
--
ALTER TABLE `village`
  ADD PRIMARY KEY (`id`),
  ADD KEY `village_ibfk_1` (`owner_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `army`
--
ALTER TABLE `army`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT dla tabeli `authority`
--
ALTER TABLE `authority`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT dla tabeli `buildings`
--
ALTER TABLE `buildings`
  MODIFY `building_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT dla tabeli `buildings_queue`
--
ALTER TABLE `buildings_queue`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=134;

--
-- AUTO_INCREMENT dla tabeli `recruitment_queue`
--
ALTER TABLE `recruitment_queue`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT dla tabeli `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT dla tabeli `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT dla tabeli `village`
--
ALTER TABLE `village`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `role_authority`
--
ALTER TABLE `role_authority`
  ADD CONSTRAINT `fk_role_has_authority_authority1` FOREIGN KEY (`authority_id`) REFERENCES `authority` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_role_has_authority_role` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ograniczenia dla tabeli `user_role`
--
ALTER TABLE `user_role`
  ADD CONSTRAINT `fk_role_has_user_role1` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_role_has_user_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ograniczenia dla tabeli `village`
--
ALTER TABLE `village`
  ADD CONSTRAINT `village_ibfk_1` FOREIGN KEY (`owner_id`) REFERENCES `user` (`id`) ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
