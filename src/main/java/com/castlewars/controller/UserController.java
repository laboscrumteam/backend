package com.castlewars.controller;

import com.castlewars.entity.user.SimpleUser;
import com.castlewars.entity.user.User;
import com.castlewars.repository.UserRepository;
import com.castlewars.repository.VillageRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * Created by Michał Daniel (michal.mateusz.daniel@gmail.com)
 */
@Slf4j
@Controller
@RequestMapping("${app.endpoint.user}")
public class UserController {

  @Autowired
  private UserRepository userRepository;
  @Autowired
  private VillageRepository villageRepository;
  
  @GetMapping
  public ResponseEntity getUserInfo() {
    log.debug("REQ [GET] : user ");

    Authentication auth = SecurityContextHolder.getContext().getAuthentication();

    String name = auth.getName();
    
    User user = userRepository.findByUsername(name);
    
    Long points = villageRepository.countPoints(user.getId());
    
    SimpleUser simpleUser = new SimpleUser(user);
    simpleUser.setPoints(points);

    return ResponseEntity.ok(simpleUser);
  }
}
