package com.castlewars.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestAuthController {

  /*@GetMapping("${app.endpoint.index}")
  public String index() {
    return "Hello";
  }*/

  @GetMapping("${app.endpoint.testEndpoint}")
  public String testAuth() {
    return "Auth Success";
  }
}
