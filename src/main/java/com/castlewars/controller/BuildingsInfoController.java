package com.castlewars.controller;

import com.castlewars.buildings.BuildingsProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * Created by Michał Daniel (michal.mateusz.daniel@gmail.com)
 */
@Slf4j
@Controller
@RequestMapping("${app.endpoint.buildinginfo}")
public class BuildingsInfoController {
  
  @Autowired
  private BuildingsProperties prop; 

  @GetMapping
  public ResponseEntity getBuildingData(){
    
    return ResponseEntity.ok(prop.getBuildings());
    
  }
}
