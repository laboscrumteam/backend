package com.castlewars.controller;

import com.castlewars.entity.user.User;
import com.castlewars.entity.village.SimpleVillage;
import com.castlewars.entity.village.Village;
import com.castlewars.entity.village.exceptions.VillageNotFoundException;
import com.castlewars.repository.UserRepository;
import com.castlewars.repository.VillageRepository;
import com.castlewars.service.VillageService;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Michał Daniel (michal.mateusz.daniel@gmail.com)
 */
@Slf4j
@Controller
@RequestMapping("${app.endpoint.village}")
public class VillageController {

  @Autowired
  private VillageService service;

  @GetMapping
  public ResponseEntity getVillage() {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();

    String name = auth.getName();
    List<? extends Object> vill = service.getVillagesList(name);

    return ResponseEntity.ok(vill);
  }

  @GetMapping("/{id}")
  public ResponseEntity getVillageInfo(@PathVariable("id") Long villageId) {

    Authentication auth = SecurityContextHolder.getContext().getAuthentication();

    String name = auth.getName();

    try {
      Optional vill = service.getVillage(villageId, name);

      return ResponseEntity.ok(vill.get());
    } catch (VillageNotFoundException ex) {
      return ResponseEntity.badRequest().body(ex.getMessage());
    }

  }

  @GetMapping("/{id}/resources")
  public ResponseEntity getVillageResources(@PathVariable("id") Long villageId) {

    Authentication auth = SecurityContextHolder.getContext().getAuthentication();

    String name = auth.getName();

    try {
      Optional vill = service.getVillage(villageId, name);

      if (vill.get() instanceof Village) {
        Village village = (Village) vill.get();
        Map<String, Long> resources = new HashMap<>();
        resources.put("wood", village.getWood());
        resources.put("iron", village.getIron());
        resources.put("clay", village.getClay());
        resources.put("population", village.getPopulation());
        return ResponseEntity.ok(resources);
      }

      return ResponseEntity.ok(vill.get());
    } catch (VillageNotFoundException ex) {
      return ResponseEntity.badRequest().body(ex.getMessage());
    }

  }

}
