package com.castlewars.controller;

import com.castlewars.entity.user.SimpleUser;
import com.castlewars.entity.user.User;
import com.castlewars.entity.enums.UserStatus;
import com.castlewars.repository.UserRepository;
import com.castlewars.service.VillageService;
import com.castlewars.service.impl.UserRoleServiceImpl;
import com.castlewars.validate.UserValidator;
import com.castlewars.validate.exceptions.ValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Michał Daniel (michal.mateusz.daniel@gmail.com)
 */
@Controller
public class RegisterController {

  private static final Logger LOGGER = LoggerFactory.getLogger(RegisterController.class);
  @Autowired
  private UserRepository userRepository;

  @Autowired
  private UserRoleServiceImpl roleService;

  @Autowired
  private VillageService villageService;

  @Autowired
  private UserValidator userValidator;

  @GetMapping("${app.endpoint.register}")
  public @ResponseBody
  ResponseEntity<SimpleUser> addNewUser(@RequestParam String name,
      @RequestParam String email,
      @RequestParam String password) throws ValidationException {
    LOGGER.info("REQ [GET]: register ? name = {} & email = {} & password = {}",
        name, email, "secret");

    User user = createRegisteredUser(name, email, password);

    userValidator.isValid(user);

    encodePassword(user);

    userRepository.save(user);
    
    villageService.createAsyncStartVillage(user.getId(), user.getName());

    roleService.setUserPrivilages(user.getId());

    return ResponseEntity.ok(new SimpleUser(user));
  }

  @ExceptionHandler(ValidationException.class)
  public @ResponseBody
  ResponseEntity handleValidationException(ValidationException ex) {
    return ResponseEntity.badRequest().body(ex.convertToObject());
  }

  private User encodePassword(User user) {
    BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    String password = user.getPassword();
    String encodePassword = passwordEncoder.encode(password);
    user.setPassword(encodePassword);
    return user;
  }

  private User createRegisteredUser(String name, String email, String password) {
    User user = new User();
    user.setName(name);
    user.setUsername(email);
    user.setPassword(password);
    user.setStatus(UserStatus.ACTIVE);
    user.setFailedLoginAttemptCount(0);
    return user;
  }

}
