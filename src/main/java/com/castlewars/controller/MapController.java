package com.castlewars.controller;

import com.castlewars.entity.user.User;
import com.castlewars.entity.village.SimpleVillage;
import com.castlewars.entity.village.Village;
import com.castlewars.repository.UserRepository;
import com.castlewars.repository.VillageRepository;
import java.util.List;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Michał Daniel (michal.mateusz.daniel@gmail.com)
 */
@Slf4j
@Controller
@RequestMapping("${app.endpoint.map}")
public class MapController {
  
  @Autowired
  private VillageRepository villageRepository;
  @Autowired
  private UserRepository userRepository;

  @GetMapping
  @RequestMapping("{cx}/{cy}/{size}")
  public ResponseEntity getArmy(@PathVariable("cx") Long centerX,
          @PathVariable("cy") Long centerY,
          @PathVariable("size") int size) {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    log.info("REQ [GET]: map / {} / {} / {} ",  centerX, centerY, size);
    
    Long posXL = centerX - size;
    Long posXR = centerX + size;
    
    Long posYT = centerY - size;
    Long posYB = centerY + size;

    if(posXL<0L)posXL = 0L;
    if(posXR>100L)posXL = 100L;
    if(posYT<0L)posYT = 0L;
    if(posYB>100L)posYB = 100L;
    
    SimpleVillage[][] map = new SimpleVillage[size*2][size*2];
    
    List<Village> villages = villageRepository.getVillagesBetweenLocation(posXL, posXR, posYT, posYB);
    villages.forEach((village) -> {
      Optional<User> user = userRepository.findById(village.getOwnerId());
      Long x = village.getLocationX()-centerX+size;
      Long y = village.getLocationY()-centerY+size;
      log.info("orginal: {} ; {} | converted: {} ; {}",village.getLocationX(), village.getLocationY(),
             x, y);
      map[x.intValue()][y.intValue()] = new SimpleVillage(village, user.get().getName());
    });
    return ResponseEntity.ok(map);
  }
}
