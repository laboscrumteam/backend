package com.castlewars.controller;

import com.castlewars.buildings.BuildingsProperties;
import com.castlewars.buildings.model.enums.BuildingProperty;
import com.castlewars.buildings.model.enums.BuildingType;
import com.castlewars.entity.building.queue.BuildingQueue;
import com.castlewars.entity.village.Village;
import com.castlewars.entity.village.exceptions.VillageNotFoundException;
import com.castlewars.repository.BuildingQueueRepository;
import com.castlewars.service.VillageService;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * BURDEL CODE Created by Michał Daniel (michal.mateusz.daniel@gmail.com)
 */
@Slf4j
@Controller
@RequestMapping("${app.endpoint.buildings}")
public class BuldingsController {

  private final Map<String, Long> buildingsId = new HashMap<>();

  @Autowired
  private BuildingsProperties prop;

  @Autowired
  private VillageService service;

  @Autowired
  private BuildingQueueRepository queueRepository;

  //TODO move to config or restrict in database fieldas
  public BuldingsController() {

    buildingsId.put("headquadrates", 1L);
    buildingsId.put("barracks", 2L);
    buildingsId.put("timbercamp", 3L);
    buildingsId.put("claypit", 4L);
    buildingsId.put("ironmine", 5L);
    buildingsId.put("centralsquare", 6L);
    buildingsId.put("warehouse", 7L);
    buildingsId.put("wall", 8L);
    buildingsId.put("farm", 9L);
  }

  //TODO: cut to more generic functions
  @PostMapping("/{id}/{building}")
  public ResponseEntity newBuildingQueue(@PathVariable("id") Long villageId,
          @PathVariable("building") String building) {

    Authentication auth = SecurityContextHolder.getContext().getAuthentication();

    String name = auth.getName();

    try {
      Optional vill = service.getVillage(villageId, name);

      if (vill.get() instanceof Village) {
        //TODO: check queue building to make sure naxt lvl is currently next to build 
        Village village = (Village) vill.get();

        BuildingType buildingType = strToBuildingType(building);
        //start time is currenty now
        Long currentLvl = getCurrentLvl(villageId, village, buildingType);
        Long startTime = getStartTime(villageId, buildingType);
        Long nextLvl = currentLvl + 1L;

        Long headquartersLvl = village.getHeadquarters();

        if (headquartersLvl < nextLvl && !buildingType.equals(BuildingType.HEADQUARTERS)) {
          return ResponseEntity.badRequest().body("Headquadrates must have a higher level");
        }

        //TODO: make exceptions
        if (Objects.equals(prop.getBuilding(buildingType).getMaxLvl(), currentLvl)) {
          return ResponseEntity.badRequest().body("Max lvl building");
        }

        Long needIron = prop.getBuilding(buildingType)
                .getValue(nextLvl, BuildingProperty.IRON_COST);
        Long needWood = prop.getBuilding(buildingType)
                .getValue(nextLvl, BuildingProperty.WOOD_COST);
        Long needClay = prop.getBuilding(buildingType)
                .getValue(nextLvl, BuildingProperty.CLAY_COST);
        Long needPopulation = prop.getBuilding(buildingType)
                .getValue(nextLvl, BuildingProperty.PEOPLE_COST);
        Long maxPopulation = prop.getBuilding(BuildingType.FARM)
                .getValue(village.get(BuildingType.FARM), BuildingProperty.BONUS);

        if (village.getWood() <= needWood
                || village.getIron() <= needIron
                || village.getClay() <= needClay
                || maxPopulation <= village.getPopulation() + needPopulation) {
          log.info("Wood {} <= {} " + (village.getWood() <= needWood), village.getWood(), needWood);
          log.info("Iron {} <= {} " + (village.getIron() <= needIron), village.getIron(), needIron);
          log.info("Clay {} <= {} " + (village.getClay() <= needClay), village.getClay(), needClay);
          log.info("Population {} <= {} " + (maxPopulation <= village.getPopulation()
                  + needPopulation), maxPopulation, village.getPopulation() + needPopulation);
          return ResponseEntity.badRequest().body("Not enough resourcess");
        }
        village.setIron(village.getIron() - needIron);
        village.setWood(village.getIron() - needWood);
        village.setClay(village.getClay() - needClay);
        village.setPopulation(village.getPopulation() + needPopulation);

        service.updateAsyncVillage(village);

        BuildingQueue bq = new BuildingQueue();
        bq.setVillageId(villageId);
        bq.setBuildingId(building);
        Long buildTime = countBuildTime(buildingType, nextLvl, village);

        bq.setStartTimestamp(startTime);
        bq.setEndTimestamp(startTime + buildTime);
        if (startTime + buildTime < startTime) {
          log.error("Critic sum kurwa error.");
        }
        bq.setNextLvl(nextLvl);
        bq.setReceived(false);

        queueRepository.save(bq);
        return ResponseEntity.ok(vill.get());
      } else {
        //TODO: make exceptions 
        return ResponseEntity.badRequest().body("Only owner can build.");
      }

    } catch (VillageNotFoundException ex) {
      return ResponseEntity.badRequest().body(ex.getMessage());
    }

  }

  private Long countBuildTime(BuildingType buildingType, Long nextLvl, Village village) {
    double buildTime = prop.getBuilding(buildingType)
            .getValue(nextLvl, BuildingProperty.BUILD_TIME) * 10 * 60;//1000ms in 1 sec, 60sec in 1 min
    //use time bonus
    Long bonus = prop.getBuilding(BuildingType.HEADQUARTERS)
            .getValue(village.getHeadquarters(), BuildingProperty.BONUS);
    buildTime = Math.round(buildTime * bonus);
    return new Double(buildTime).longValue();
  }

  private Long getCurrentLvl(Long villageId, Village village, BuildingType buildingType) {
    List<BuildingQueue> queues = queueRepository.getBuildingQueueByVillageId(villageId);
    long currentLvl = village.get(buildingType);
    for (BuildingQueue queue : queues) {
      BuildingType type = strToBuildingType(queue.getBuildingId());
      if (!queue.isReceived() && type.equals(buildingType)) {
        currentLvl = currentLvl < queue.getNextLvl() ? queue.getNextLvl() : currentLvl;
      }
    }
    return currentLvl;
  }

  //TODO: cut to more generic functions
  @GetMapping("/{id}")
  public ResponseEntity getBuildingQueue(@PathVariable("id") Long villageId) {
    log.info("GET - buiding queue");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();

    String name = auth.getName();

    try {
      Optional vill = service.getVillage(villageId, name);

      if (vill.get() instanceof Village) {

        //Village village = (Village) vill.get();
        List<BuildingQueue> queues = queueRepository.getBuildingQueueByVillageId(villageId);
        long currentTime = System.currentTimeMillis();

        /*List<BuildingQueue> queues = buildingQueueByVillageId.isPresent()
        ? Collections.singletonList(buildingQueueByVillageId.get())
        : Collections.emptyList();*/
        Map<String, List<BuildingQueue>> groupedQueues = new HashMap<>();

        groupByStatus(queues, groupedQueues, currentTime);

        return ResponseEntity.ok(groupedQueues);
      } else {
        //TODO: make exceptions 
        return ResponseEntity.badRequest().body("Only owner can received this information.");
      }

    } catch (VillageNotFoundException ex) {
      return ResponseEntity.badRequest().body(ex.getMessage());
    }

  }

  @GetMapping("/{vill_id}/received/{queue_id}")
  public ResponseEntity receivedBuilding(@PathVariable("vill_id") Long villageId,
          @PathVariable("queue_id") Long queueId) {

    Authentication auth = SecurityContextHolder.getContext().getAuthentication();

    String name = auth.getName();

    try {
      Optional vill = service.getVillage(villageId, name);
      if (vill.get() instanceof Village) {
        BuildingQueue bq = queueRepository.findById(queueId).get();
        if (!Objects.equals(villageId, bq.getVillageId())) {
          return ResponseEntity.badRequest().body("This is not the same village");
        }
        bq.setReceived(true);
        queueRepository.updateReceived(bq.getId(), true);
        Village village = (Village) vill.get();
        BuildingType type = BuildingType.valueOf(bq.getBuildingId());
        Long points = village.getPoints();
        Long addPoints = prop.getBuilding(type)
                .getValue(bq.getNextLvl(), BuildingProperty.POINTS_PER_LVL);
        points += addPoints;
        village.setPoints(points);
        Long buildingLvl = village.get(type);
        buildingLvl = buildingLvl < bq.getNextLvl() ? bq.getNextLvl() : buildingLvl;
        village.set(type, buildingLvl);

        service.updateAsyncVillage(village);

      } else {
        return ResponseEntity.badRequest().body("This is not your village");
      }
    } catch (VillageNotFoundException ex) {
      log.error(ex.getMessage());
    }

    return ResponseEntity.ok("OK");
  }

  @GetMapping("/{vill_id}/nextlvl")
  public ResponseEntity nextlvlBuildings(@PathVariable("vill_id") Long villageId) {

    Authentication auth = SecurityContextHolder.getContext().getAuthentication();

    String name = auth.getName();
    Map<String, Long> nextlvl = new HashMap<>();

    try {
      Optional vill = service.getVillage(villageId, name);
      if (vill.get() instanceof Village) {
        Village village = (Village) vill.get();

        for (BuildingType value : BuildingType.values()) {
          Long currentLvl = getCurrentLvl(villageId, village, value);
          if (currentLvl++ <= prop.getBuilding(value).getMaxLvl()) {
            nextlvl.put(value.name(), currentLvl++);
          }
        }
      } else {
        return ResponseEntity.badRequest().body("This is not your village");
      }
    } catch (VillageNotFoundException ex) {
      log.error(ex.getMessage());
    }

    return ResponseEntity.ok(nextlvl);
  }

  private void groupByStatus(List<BuildingQueue> queues, Map<String, List<BuildingQueue>> groupedQueues, long currentTime) {
    queues.forEach((buildingQueue) -> {
      Long endTimestamp = buildingQueue.getEndTimestamp();
      if (buildingQueue.isReceived()) {
        //received building can be expired
        List<BuildingQueue> value = groupedQueues.get("received");
        value = value == null ? new LinkedList<>() : value;
        value.add(buildingQueue);
        groupedQueues.put("received", value);
      } else if (endTimestamp <= currentTime) {
        //building end and not received yet
        List<BuildingQueue> value = groupedQueues.get("ended");
        value = value == null ? new LinkedList<>() : value;
        value.add(buildingQueue);
        groupedQueues.put("ended", value);
      } else {
        //building in progress
        List<BuildingQueue> value = groupedQueues.get("in_progress");
        value = value == null ? new LinkedList<>() : value;
        value.add(buildingQueue);
        groupedQueues.put("in_progress", value);
      }
    });
  }

  private BuildingType strToBuildingType(String type) {
    switch (type) {
      case "HEADQUARTERS":
        return BuildingType.HEADQUARTERS;
      case "BARRACKS":
        return BuildingType.BARRACKS;
      case "TIMBER_CAMP":
        return BuildingType.TIMBER_CAMP;
      case "CLAY_PIT":
        return BuildingType.CLAY_PIT;
      case "IRON_MINE":
        return BuildingType.IRON_MINE;
      case "CENTRAL_SQUARE":
        return BuildingType.CENTRAL_SQUARE;
      case "WAREHOUSE":
        return BuildingType.WAREHOUSE;
      case "WALL":
        return BuildingType.WALL;
      case "FARM":
        return BuildingType.FARM;
    }
    return null;
  }

  private String getBuilingNameFromId(Long id) {
    for (Map.Entry<String, Long> entry : buildingsId.entrySet()) {
      if (entry.getValue().equals(id)) {
        return entry.getKey();
      }
    }
    return "NOT FOUND";
  }

  private Long getStartTime(Long villageId, BuildingType buildingType) {
    List<BuildingQueue> queues = queueRepository.getBuildingQueueByVillageId(villageId);
    Long endTime = System.currentTimeMillis();
    for (BuildingQueue queue : queues) {
      BuildingType type = strToBuildingType(queue.getBuildingId());
      if (!queue.isReceived() && type.equals(buildingType)) {
        endTime = endTime < queue.getEndTimestamp() ? queue.getEndTimestamp() : endTime;
      }
    }
    return endTime;
  }

}
