package com.castlewars.controller;

import com.castlewars.buildings.BuildingsProperties;
import com.castlewars.buildings.model.enums.BuildingProperty;
import com.castlewars.buildings.model.enums.BuildingType;
import com.castlewars.entity.cost.Cost;
import com.castlewars.entity.military.Army;
import com.castlewars.entity.military.RecruitmentQueue;
import com.castlewars.entity.user.User;
import com.castlewars.entity.village.SimpleVillage;
import com.castlewars.entity.village.Village;
import com.castlewars.entity.village.exceptions.VillageNotFoundException;
import com.castlewars.repository.RecruitmentQueueRepository;
import com.castlewars.repository.UserRepository;
import com.castlewars.repository.VillageRepository;
import com.castlewars.service.ArmyService;
import com.castlewars.service.VillageService;
import com.castlewars.units.UnitsProperties;
import com.castlewars.units.model.UnitProperty;
import com.castlewars.units.model.UnitType;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Michał Daniel (michal.mateusz.daniel@gmail.com)
 */
@Slf4j
@Controller
@RequestMapping("${app.endpoint.army}")
public class ArmyController {

  @Autowired
  private ArmyService armyService;
  @Autowired
  private VillageService villageService;
  @Autowired
  private UnitsProperties unitsProperties;
  @Autowired
  private BuildingsProperties buildingsProperties;
  @Autowired
  private RecruitmentQueueRepository recruitmentRepository;

  @GetMapping
  @RequestMapping("/{id}")
  public ResponseEntity getArmy(@PathVariable("id") Long villageId) {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();

    String name = auth.getName();

    try {
      //TODO: move this body to suitable service
      Optional village = villageService.getVillage(villageId, name);
      if (village.get() instanceof SimpleVillage) {
        return ResponseEntity.badRequest().body("This is not your village");
      }
      Village vill = (Village) village.get();
      Optional<Army> armyOptional = armyService.getArmyForVillage(villageId);
      Army army = armyOptional.get();
      List<RecruitmentQueue> recruitmentQueues = recruitmentRepository
              .getAllRecruitmentQueueForVillage(villageId);

      if (!recruitmentQueues.isEmpty()) {
        List<RecruitmentQueue> recruitmentQueue = recruitmentQueues;

        List<RecruitmentQueue> filtered = recruitmentQueue
                .stream()
                .filter(r -> Objects.equals(r.getStartRecruit(), Boolean.TRUE)
                && Objects.equals(r.getEndRecruit(), Boolean.FALSE))
                .collect(Collectors.toList());
        Long currentTime = System.currentTimeMillis();
        for (RecruitmentQueue rq : filtered) {
          Long deltaTime = currentTime - rq.getLastUpdate();
          Long toRecruit = rq.getRecruitValue() - rq.getRecruitNow();
          UnitType type = UnitType.valueOf(rq.getUnitId());

          Long recruitTime = unitsProperties.getUnitProperty(type, UnitProperty.RECRUIT_TIME);
          Long bonusBarracks = buildingsProperties.getBuilding(BuildingType.BARRACKS)
                  .getValue(vill.get(BuildingType.BARRACKS), BuildingProperty.BONUS);
          Long realRecruitTime = new Double(recruitTime * 10 * bonusBarracks).longValue();
          
          if (realRecruitTime == 0L) {
            log.debug("Time is 0, but can't do that, recTime={}, bonus={}, res={}", recruitTime, bonusBarracks,new Double(Math.round((recruitTime * 1000) * (bonusBarracks / 100))).longValue());
          }

          Long recruitUnits = realRecruitTime > 0 ? new Double(deltaTime / realRecruitTime).longValue() : 0;// recruit time is in seconds
          Boolean recruitAll = recruitUnits >= toRecruit;
          recruitUnits = recruitAll ? toRecruit : recruitUnits;

          rq.setEndRecruit(recruitAll);
          Long lastUpadate = rq.getLastUpdate() + (recruitUnits * realRecruitTime);
          rq.setLastUpdate(lastUpadate);
          rq.setRecruitNow(rq.getRecruitNow() + recruitUnits);
          army.set(type, army.get(type) + recruitUnits);
          updateRecruitQueue(rq);

          if (recruitAll) {
            List<RecruitmentQueue> unstarted = recruitmentQueue
                    .stream()
                    .filter(r -> Objects.equals(r.getStartRecruit(), Boolean.FALSE)
                    && Objects.equals(r.getEndRecruit(), Boolean.FALSE))
                    .sorted(Comparator.comparingLong(RecruitmentQueue::getId))
                    .collect(Collectors.toList());

            for (RecruitmentQueue rqu : unstarted) {
              rqu.setStartRecruit(Boolean.TRUE);
              rqu.setStartTimestamp(lastUpadate);
              deltaTime = currentTime - lastUpadate;
              type = UnitType.valueOf(rqu.getUnitId());
              recruitTime = unitsProperties.getUnitProperty(type, UnitProperty.RECRUIT_TIME);
              realRecruitTime = new Double(recruitTime * 10 * bonusBarracks).longValue();
              toRecruit = rqu.getRecruitValue() - rqu.getRecruitNow();
              recruitUnits = new Double(deltaTime / realRecruitTime).longValue();
              recruitAll = recruitUnits >= toRecruit;
              recruitUnits = recruitUnits >= toRecruit ? toRecruit : recruitUnits;

              rqu.setEndRecruit(recruitAll);
              lastUpadate = lastUpadate + (recruitUnits * realRecruitTime);
              rqu.setLastUpdate(lastUpadate);
              rqu.setRecruitNow(rqu.getRecruitNow() + recruitUnits);
              army.set(type, army.get(type) + recruitUnits);
              updateRecruitQueue(rqu);

              if (!recruitAll) {
                break;
              }
            }
          }

        }
      }
      armyService.update(army);
      return ResponseEntity.ok(army);
    } catch (VillageNotFoundException ex) {
      log.error("Village with ID : {} doesn't exist.", villageId);
      return ResponseEntity.badRequest().body("Village not found");
    }
  }

  @GetMapping
  @RequestMapping("/{id}/queue")
  public ResponseEntity getRecruitQueue(@PathVariable("id") Long villageId) {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();

    String name = auth.getName();

    try {
      //TODO: move this body to suitable service
      Optional village = villageService.getVillage(villageId, name);
      if (village.get() instanceof SimpleVillage) {
        return ResponseEntity.badRequest().body("This is not your village");
      }
      Village vill = (Village) village.get();

      List<RecruitmentQueue> allQueues = recruitmentRepository.getAllRecruitmentQueueForVillage(villageId);

      allQueues = allQueues.stream().filter(q -> Objects.equals(q.getEndRecruit(), Boolean.FALSE)).collect(Collectors.toList());

      return ResponseEntity.ok(allQueues);
    } catch (VillageNotFoundException ex) {
      log.error("Village with ID : {} doesn't exist.", villageId);
      return ResponseEntity.badRequest().body("Village not found");
    }
  }

  @PostMapping
  @RequestMapping("/{id}/recruit/{unitId}/{numberOfRecruits}")
  public ResponseEntity recruitArmy(@PathVariable("id") Long villageId,
          @PathVariable("unitId") String unitId,
          @PathVariable("numberOfRecruits") Long numberOfRecruits) {
    log.debug("REQ [POST]: army / {} / recruit / {} / {}", villageId, unitId, numberOfRecruits);
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();

    String name = auth.getName();

    try {
      Optional vill = villageService.getVillage(villageId, name);
      if (vill.get() instanceof SimpleVillage) {
        return ResponseEntity.badRequest().body("This is not your village");
      }
      //TODO: move code to service
      Village village = (Village) vill.get();
      UnitType unitType = UnitType.valueOf(unitId);

      Cost recruitmentCost = countRecruitmentCost(unitType, numberOfRecruits);

      if (!canRecruit(village, recruitmentCost, unitType)) {
        return ResponseEntity.badRequest().body("Not enaugth resourcess or barracks lvl is to low.");
      }
      Long timestamp = 0L;

      RecruitmentQueue queue = createRecruitmentQueue(unitId, villageId, timestamp, numberOfRecruits);

      List<RecruitmentQueue> recruitmentQueues = recruitmentRepository
              .getAllRecruitmentQueueForVillage(villageId);

      if (recruitmentQueues.isEmpty()) {
        queue.setStartRecruit(Boolean.TRUE);
        queue.setStartTimestamp(System.currentTimeMillis());
        queue.setLastUpdate(System.currentTimeMillis());

      } else {
        List<RecruitmentQueue> rq = recruitmentQueues;
        //is there any more in the process
        List<RecruitmentQueue> filtered = rq
                .stream()
                .filter(r -> Objects.equals(r.getStartRecruit(), Boolean.TRUE)
                && Objects.equals(r.getEndRecruit(), Boolean.FALSE))
                .collect(Collectors.toList());
        if (filtered.isEmpty()) {
          queue.setStartRecruit(Boolean.TRUE);
          queue.setStartTimestamp(System.currentTimeMillis());
          queue.setLastUpdate(System.currentTimeMillis());
        }

      }

      updateVillageResources(village, recruitmentCost);

      recruitmentRepository.save(queue);
      return ResponseEntity.ok(queue);
    } catch (VillageNotFoundException ex) {
      log.error("Village with ID : {} doesn't exist.", villageId);
      return ResponseEntity.badRequest().body("Village not found");
    }
  }

  //TODO: move methods to appropriate classes
  private RecruitmentQueue createRecruitmentQueue(String unitId, Long villageId, Long timestamp, Long numberOfRecruits) {
    RecruitmentQueue queue = new RecruitmentQueue();
    queue.setRecruitNow(0L);
    queue.setUnitId(unitId);
    queue.setVillageId(villageId);
    queue.setStartTimestamp(timestamp);
    queue.setLastUpdate(timestamp);
    queue.setRecruitValue(numberOfRecruits);
    queue.setEndRecruit(Boolean.FALSE);
    queue.setStartRecruit(Boolean.FALSE);
    return queue;
  }

  private Cost countRecruitmentCost(UnitType unitType, Long numberOfRecruits) {
    Cost result = new Cost();
    result.setWood(unitsProperties
            .getUnitProperty(unitType, UnitProperty.WOOD_COST) * numberOfRecruits);
    result.setClay(unitsProperties
            .getUnitProperty(unitType, UnitProperty.CLAY_COST) * numberOfRecruits);
    result.setIron(unitsProperties
            .getUnitProperty(unitType, UnitProperty.IRON_COST) * numberOfRecruits);
    result.setPeople(unitsProperties
            .getUnitProperty(unitType, UnitProperty.PEOPLE_COST) * numberOfRecruits);

    return result;
  }

  private boolean canRecruit(Village village, Cost cost, UnitType unitType) {
    Long maxPopulation = buildingsProperties.getBuilding(BuildingType.FARM).getValue(village.getFarm(), BuildingProperty.BONUS);
    if (village.getWood() < cost.getWood()) {
      return false;
    } else if (village.getClay() < cost.getClay()) {
      return false;
    } else if (village.getIron() < cost.getIron()) {
      return false;
    } else if (maxPopulation < cost.getPeople()) {
      return false;
    }

    Long minBarracks = unitsProperties.getUnitProperty(unitType, UnitProperty.MIN_BARRACKS_LVL);
    if (village.get(BuildingType.BARRACKS) < minBarracks) {
      return false;
    }

    return true;
  }

  private void updateVillageResources(Village village, Cost cost) {

    Long newWood = village.getWood() - cost.getWood();
    village.setWood(newWood);

    Long newClay = village.getClay() - cost.getClay();
    village.setClay(newClay);

    Long newIron = village.getIron() - cost.getIron();
    village.setIron(newIron);

    Long newPopulation = village.getPopulation() + cost.getPeople();
    village.setPopulation(newPopulation);

    villageService.updateAsyncVillage(village);

  }

  private void updateRecruitQueue(RecruitmentQueue rq) {
    recruitmentRepository.update(rq.getId(),
            rq.getEndRecruit(),
            rq.getStartRecruit(),
            rq.getStartTimestamp(),
            rq.getRecruitNow(),
            rq.getLastUpdate());

  }

}
