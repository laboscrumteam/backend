package com.castlewars.repository;

import com.castlewars.entity.military.RecruitmentQueue;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Michał Daniel (michal.mateusz.daniel@gmail.com)
 */
@Repository
public interface RecruitmentQueueRepository extends JpaRepository<RecruitmentQueue, Long> {

    @Query("from RecruitmentQueue r where r.villageId=:villageId")
    List<RecruitmentQueue> getAllRecruitmentQueueForVillage(@Param("villageId") Long villageId);
    
    @Modifying
    @Transactional
    @Query(value = "UPDATE recruitment_queue SET end_recruit=:end_recruit, "
            + "recruit_now=:recruit_now, last_update=:last_update, "
            + "start_recruit=:start_recruit, start_timestamp=:start_timestamp "
            + "where id=:id",
            nativeQuery = true)
    void update(@Param("id") Long id,
           @Param("end_recruit") Boolean endRecruit,
           @Param("start_recruit") Boolean startRecruit,
           @Param("start_timestamp") Long startTimestamp,
           @Param("recruit_now") Long recruitNow,
           @Param("last_update") Long lastUpdate);
    

}
