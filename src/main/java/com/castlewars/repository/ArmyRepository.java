package com.castlewars.repository;

import com.castlewars.entity.military.Army;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Michał Daniel (michal.mateusz.daniel@gmail.com)
 */
@Repository
public interface ArmyRepository extends JpaRepository<Army, Long> {

    @Query("from Army where villageId =:id")
    Optional<Army> getArmyForVillage(@Param("id") Long villageId);

    @Modifying
    @Transactional
    @Query(value = "UPDATE army SET spear_fighter=:spearFighter, "
            + "swordsman=:swordsman, archer=:archer, cavalary=:cavalary "
            + "where village_id=:id",
            nativeQuery = true)
    void updateArmy(@Param("id") Long villageId,
            @Param("spearFighter") Long spearFighter,
            @Param("swordsman") Long swordsman,
            @Param("archer") Long archer,
            @Param("cavalary") Long cavalary);

}
