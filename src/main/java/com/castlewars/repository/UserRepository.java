package com.castlewars.repository;

import com.castlewars.entity.user.User;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * Created by Michał Daniel (michal.mateusz.daniel@gmail.com)
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

  @Query("from User u where u.id =:idin")
  Optional<User> getUserByID(@Param("idin") int id);

  @Query("from User u where u.username =:email")
  Optional<User> checkEmail(@Param("email") String email);

  @Query("from User u where u.name =:name")
  Optional<User> checkName(@Param("name") String nickname);
  
  @Query("select u.id from User u where u.name =:name")
  Optional<Long> getUserId(@Param("name") String nickname);

  User findByUsername(String username);

}
