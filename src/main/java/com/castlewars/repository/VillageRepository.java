package com.castlewars.repository;

import com.castlewars.entity.village.Village;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Michał Daniel (michal.daniel@makolab.com)
 */
public interface VillageRepository extends JpaRepository<Village, Long> {

  //get village id
  @Query("SELECT v.id from Village v join User u on v.ownerId=u.id where u.username =:name")
  public Optional<Long> getVillagesByOwnerName(@Param("name") String name);
  
  @Query("from Village where locationX =:lx AND locationY=:ly")
  public Optional<Village> getVillagesByLocation(@Param("lx") Long lx,
          @Param("ly") Long ly);
  
  @Query("from Village where locationX >=:lx AND locationY>=:ly AND locationX <:lx2 AND locationY<:ly2")
  public List<Village> getVillagesBetweenLocation(@Param("lx") Long lx,
          @Param("lx2") Long lx2,
          @Param("ly") Long ly,
          @Param("ly2") Long ly2);

  @Query("SELECT sum(points) from Village v where v.ownerId =:id")
  public Long countPoints(@Param("id") Long id);

  @Modifying
  @Transactional
  @Query(value = "UPDATE Village SET wood=:wood, clay=:clay, "
          + "iron=:iron, "
          + "last_timestamp=:timestamp, "
          + "population=:population, "
          + "owner_id=:owner, "
          + "name=:name, "
          + "headquarters=:headquarters, "
          + "central_square=:centralSquare, "
          + "barracks=:barracks, "
          + "timber_camp=:timberCamp, "
          + "clay_pit=:clayPit, "
          + "iron_mine=:ironMine, "
          + "warehouse=:warehouse, "
          + "wall=:wall, "
          + "farm=:farm, "
          + "points=:points, "
          + "loyalty=:loyalty"
          + " where ID=:id",
          nativeQuery = true)
  public void updateVillage(@Param("id") Long id,
          @Param("wood") long wood,
          @Param("clay") long clay,
          @Param("iron") long iron,
          @Param("timestamp") long lastTimestamp,
          @Param("population") long population,
          @Param("owner") Long ownerId,
          @Param("name")String name,
          @Param("headquarters")Long headquarters,
          @Param("centralSquare")Long centralSquare,
          @Param("barracks")Long barracks,
          @Param("timberCamp")Long timberCamp,
          @Param("clayPit")Long clayPit,
          @Param("ironMine")Long ironMine,
          @Param("warehouse")Long warehouse,
          @Param("wall")Long wall,
          @Param("farm")Long farm,
          @Param("points")Long points,
          @Param("loyalty")long loyalty);

}
