package com.castlewars.repository;

import com.castlewars.entity.user.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.query.Param;

/**
 * Created by Michał Daniel (michal.mateusz.daniel@gmail.com)
 */
@Repository
public interface UserRoleRepository extends JpaRepository<UserRole, Long> {

  @Modifying
  @Query(value = "INSERT INTO user_role (role_id, user_id) SELECT id, :user_id "
          + "FROM castlewars.role WHERE name = 'User'",
          nativeQuery = true)
  void setUserPrivilages(@Param("user_id") Long userID);
}
