package com.castlewars.repository;

import com.castlewars.entity.building.queue.BuildingQueue;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Michał Daniel (michal.daniel@makolab.com)
 */
@Repository
public interface BuildingQueueRepository extends JpaRepository<BuildingQueue, Long> {

  @Query("from BuildingQueue where villageId =:id")
  public List<BuildingQueue> getBuildingQueueByVillageId(@Param("id") Long id);

  @Modifying
  @Transactional
  @Query(value = "UPDATE buildings_queue SET received=:b where ID=:id",
          nativeQuery = true)
  public void updateReceived(@Param("id") Long id, @Param("b")boolean b);

}
