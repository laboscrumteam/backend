package com.castlewars.map;

import com.castlewars.buildings.model.Pair;
import com.castlewars.entity.village.Village;
import com.castlewars.repository.VillageRepository;
import com.castlewars.service.VillageService;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Michał Daniel (michal.mateusz.daniel@gmail.com)
 */
@Slf4j
@Component
public class MapPositionGenerator {

  @Autowired
  private MapProperties properties;
  private Integer acctualStepGenerating;
  @Autowired
  private VillageRepository villageRepository;

  @PostConstruct
  public void init() {

    checkAcctualMapStep();
    log.debug("End initialize map information from database.");

  }

  public Pair<Long, Long> generate() {
    checkAcctualMapStep();
    Pair currentGeneration = properties.getOrderCreationStrategy().get(acctualStepGenerating);
    Long posX = new Double((int) currentGeneration.getKey() * 10).longValue();
    Long posY = new Double((int) currentGeneration.getValue() * 10).longValue();

    Long villageX;
    Long villageY;
    Optional<Village> villagesByLocation;
    do {
      villageX = ThreadLocalRandom.current().nextLong(posX, posX + 10);
      villageY = ThreadLocalRandom.current().nextLong(posY, posY + 10);
      villagesByLocation = villageRepository.getVillagesByLocation(villageX, villageY);

    } while (villagesByLocation.isPresent());

    log.info("New village can  be generated in pos x:{}, y:{} for strategy step:{} (K:{},{})", villageX, villageY, acctualStepGenerating, posX, posY);

    return new Pair<>(villageX, villageY);
  }

  private void checkAcctualMapStep() {
    List<Village> all = villageRepository.findAll();
    Integer step = 0;

    for (Pair pair : properties.getOrderCreationStrategy()) {
      Long stepX = new Double((Integer) pair.getKey() * 10).longValue();
      Long stepY = new Double((Integer) pair.getValue() * 10).longValue();
      List<Village> collect = all.stream().filter(v -> v.getLocationX() >= stepX
              && v.getLocationX() < stepX + 10
              && v.getLocationY() >= stepY
              && v.getLocationY() < stepY + 10)
              .collect(Collectors.toList());
      log.debug("For pair creation stategy: {}, find {} villages.", pair, collect.size());
      if (collect.size() >= properties.getDensityVillages()) {
        step += 1;
      } else {
        break;
      }
    }
    acctualStepGenerating = step;
  }

}
