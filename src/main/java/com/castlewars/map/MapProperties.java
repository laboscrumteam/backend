package com.castlewars.map;

import com.castlewars.buildings.model.Pair;
import java.util.LinkedList;
import java.util.List;
import javax.annotation.PostConstruct;
import lombok.Getter;
import org.springframework.stereotype.Component;

/**
 * Created by Michał Daniel (michal.mateusz.daniel@gmail.com)
 */
@Component
public class MapProperties {

  @Getter
  private List<Pair> orderCreationStrategy;
  @Getter
  private final Integer densityVillages = 35;

  @PostConstruct
  public void init() {
    orderCreationStrategy = new LinkedList<>();

    orderCreationStrategy.add(new Pair(5, 5));
    orderCreationStrategy.add(new Pair(5, 6));
    orderCreationStrategy.add(new Pair(6, 5));
    orderCreationStrategy.add(new Pair(6, 6));
    orderCreationStrategy.add(new Pair(4, 5));
    orderCreationStrategy.add(new Pair(7, 6));
    orderCreationStrategy.add(new Pair(5, 7));
    orderCreationStrategy.add(new Pair(6, 4));
    orderCreationStrategy.add(new Pair(5, 4));
    orderCreationStrategy.add(new Pair(4, 4));
    orderCreationStrategy.add(new Pair(4, 6));
    orderCreationStrategy.add(new Pair(4, 7));
    orderCreationStrategy.add(new Pair(6, 7));
    orderCreationStrategy.add(new Pair(7, 7));
    orderCreationStrategy.add(new Pair(7, 5));
    orderCreationStrategy.add(new Pair(7, 4));
    //other to add
  }
}
