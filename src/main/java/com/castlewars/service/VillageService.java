package com.castlewars.service;

import com.castlewars.buildings.BuildingsProperties;
import com.castlewars.buildings.model.Pair;
import com.castlewars.buildings.model.enums.BuildingProperty;
import com.castlewars.buildings.model.enums.BuildingType;
import com.castlewars.entity.user.User;
import com.castlewars.entity.village.SimpleVillage;
import com.castlewars.entity.village.Village;
import com.castlewars.entity.village.exceptions.VillageNotFoundException;
import com.castlewars.map.MapPositionGenerator;
import com.castlewars.repository.UserRepository;
import com.castlewars.repository.VillageRepository;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * Create by Michał Daniel (michal.daniel@makolab.com)
 */
@Slf4j
@Service
public class VillageService {

  //TODO: make some block to edit village if other function can make some changes 
  @Autowired
  private VillageRepository villageRepository;
  @Autowired
  private UserRepository userRepository;
  @Autowired
  private BuildingsProperties buildingProp;
  @Autowired
  private ArmyService armyService;
  @Autowired
  private MapPositionGenerator positionGenerator;

  private static final int PRODUCTION_TIME_MS_TO_MIN = 60000;
  private static final int PRODUCTION_TIME_GENERATING_RESOURCESS = 60;

  @Async("threadPoolTaskExecutor")
  public void createAsyncStartVillage(long ownerId, String uName) {
    Village village = createStartVillage(ownerId, uName);
    saveNewVillage(village);
    armyService.createEmptyArmyForNewVillage(village.getId());
  }

  public List<? extends Object> getVillagesList(String name) {
    Optional villages = villageRepository.getVillagesByOwnerName(name);
    List<? extends Object> vill = villages.isPresent()
            ? Collections.singletonList(villages.get())
            : Collections.emptyList();
    return vill;
  }

  private void saveNewVillage(Village village) {
    villageRepository.save(village);
    log.debug("Create new village for user with ID: {}.", village.getOwnerId());
  }

  public Optional getVillage(Long villageId, String name) throws VillageNotFoundException {
    Optional<Village> village = villageRepository.findById(villageId);

    if (!village.isPresent()) {
      String msg = String.format("Village with id: %s NOT FOUND.", villageId);
      throw new VillageNotFoundException(msg);
    }

    User user = userRepository.findByUsername(name);

    Village villageObj = village.get();

    if (Objects.equals(villageObj.getOwnerId(), user.getId())) {
      //mb use big decimal ? 
      long deltaTime = getDeltaTime(villageObj);
      //long deltaTime = 10; //getDeltaTime(villageObj);
      if (deltaTime < PRODUCTION_TIME_MS_TO_MIN) {
        //not update resourcess, update only every 1m  
        return village;
      }
      int timeInMinutes = (int) deltaTime / PRODUCTION_TIME_MS_TO_MIN;

      countResoucess(villageObj, timeInMinutes);

      if (villageObj.getPoints() == 0) {
        countPoints(villageObj);
      }

      updateAsyncVillage(villageObj);

      return village;
    } else {
      return Optional.of(new SimpleVillage(villageObj, user.getName()));
    }

  }

//TODO: change all headquadrates to castle ! castle is in the name of game and this is more easy to remember
  private long getDeltaTime(Village villageObj) {
    //count village resourcess
    long currentTime = System.currentTimeMillis();
    long oldTime = villageObj.getLastTimestamp();

    return currentTime - oldTime;
  }

  //TODO: move some methods to VillageManager and VillageDataGenerator
  private Village createStartVillage(long ownerId, String uName) {
    Village village = new Village();

    village.setOwnerId(ownerId);
    createBuildings(village);
    village.setName(String.format("Wioska %s", uName));
    setStartLocation(village);
    village.setLoyalty(100);
    giveStartResources(village);
    countPeople(village);

    village.setLastTimestamp(System.currentTimeMillis());

    return village;
  }
  //TODO: move to village factory 
  //<editor-fold defaultstate="collapsed" desc="Village creation">

  private void giveStartResources(Village village) {
    village.setWood(1000);
    village.setIron(1000);
    village.setClay(1000);
  }

  private void setStartLocation(Village village) {

    Pair<Long, Long> position = positionGenerator.generate();
    village.setLocationX(position.getKey());
    village.setLocationY(position.getValue());
  }
  
  private void createBuildings(Village village) {

    village.setHeadquarters(buildingProp.getBuilding(BuildingType.HEADQUARTERS).getStartLvl());
    village.setBarracks(buildingProp.getBuilding(BuildingType.BARRACKS).getStartLvl());
    village.setCentralSquare(buildingProp.getBuilding(BuildingType.CENTRAL_SQUARE).getStartLvl());
    village.setTimberCamp(buildingProp.getBuilding(BuildingType.TIMBER_CAMP).getStartLvl());
    village.setClayPit(buildingProp.getBuilding(BuildingType.CLAY_PIT).getStartLvl());
    village.setIronMine(buildingProp.getBuilding(BuildingType.IRON_MINE).getStartLvl());
    village.setWarehouse(buildingProp.getBuilding(BuildingType.WAREHOUSE).getStartLvl());
    village.setWall(buildingProp.getBuilding(BuildingType.WALL).getStartLvl());
    village.setFarm(buildingProp.getBuilding(BuildingType.FARM).getStartLvl());

    countPoints(village);
  }
//</editor-fold>

  private void countResoucess(Village village, int multiplier) {
    Long clayPit = village.getClayPit();
    Long ironMine = village.getIronMine();
    Long timberCamp = village.getTimberCamp();

    Long producedClay = countProducedResourcess(multiplier, clayPit, BuildingType.CLAY_PIT);
    Long producedIron = countProducedResourcess(multiplier, ironMine, BuildingType.IRON_MINE);
    Long producedWood = countProducedResourcess(multiplier, timberCamp, BuildingType.TIMBER_CAMP);

    Long warehouseLvl = village.getWarehouse();
    Long maxResourcess = getMaxResourcess(warehouseLvl);

    countNewResoucess(village, producedClay, producedWood, producedIron, maxResourcess);

    village.setLastTimestamp(village.getLastTimestamp() + multiplier * PRODUCTION_TIME_MS_TO_MIN);
  }

  private void countNewResoucess(Village village, Long producedClay, Long producedWood, Long producedIron, Long maxResourcess) {
    long newClay = village.getClay() + producedClay;
    long newWood = village.getWood() + producedWood;
    long newIron = village.getIron() + producedIron;

    village.setClay(newClay > maxResourcess ? maxResourcess : newClay);
    village.setWood(newWood > maxResourcess ? maxResourcess : newWood);
    village.setIron(newIron > maxResourcess ? maxResourcess : newIron);
  }

  private Long getMaxResourcess(Long warehouseLvl) {
    return buildingProp.getBuilding(BuildingType.WAREHOUSE)
            .getValue(warehouseLvl, BuildingProperty.BONUS);
  }

  private Long countProducedResourcess(int multiplier, Long lvl, BuildingType type) {
    return new Double(Math.round(multiplier * (buildingProp.getBuilding(type)
            .getValue(lvl, BuildingProperty.BONUS) / PRODUCTION_TIME_GENERATING_RESOURCESS))).longValue();
  }

  @Async("threadPoolTaskExecutor")
  public void updateAsyncVillage(Village village) {
    updateVillage(village);
  }

  private void updateVillage(Village village) {
    villageRepository.updateVillage(village.getId(),
            village.getWood(),
            village.getClay(),
            village.getIron(),
            village.getLastTimestamp(),
            village.getPopulation(),
            village.getOwnerId(),
            village.getName(),
            village.getHeadquarters(),
            village.getCentralSquare(),
            village.getBarracks(),
            village.getTimberCamp(),
            village.getClayPit(),
            village.getIronMine(),
            village.getWarehouse(),
            village.getWall(),
            village.getFarm(),
            village.getPoints(),
            village.getLoyalty());

    log.trace("update with id = {}", village.getId());

  }

  private void countPoints(Village village) {
    Long points = 0L;
    /*buildingProp.getBuilding(BuildingType.HEADQUARTERS)
                    .getProperties()
                    .get(new Pair<>(village.getHeadquarters(), BuildingProperty.POINTS_PER_LVL))*/
    for (BuildingType type : BuildingType.values()) {
      Long buildingLvl = village.get(type);
      if (buildingLvl > 0) {
        for (int i = 1; i <= buildingLvl; i++) {
          Pair propertyKey = new Pair(new Long(i), BuildingProperty.POINTS_PER_LVL);
          points += buildingProp.getBuilding(type)
                  .getProperties()
                  .get(propertyKey);
        }
      }
    }

    village.setPoints(points);
  }

  private void countPeople(Village village) {

    int people = 0;
    for (BuildingType type : BuildingType.values()) {
      Long buildingLvl = village.get(type);
      if (buildingLvl > 0) {
        for (int i = 1; i <= buildingLvl; i++) {
          //Pair propertyKey = new Pair(i, BuildingProperty.PEOPLE_COST);
          Long peopleCost = buildingProp.getBuilding(type)
                  .getValue(buildingLvl, BuildingProperty.PEOPLE_COST);

          //    .getProperties()
          //      .get(propertyKey);
          if (peopleCost == null) {
            //log.debug(propertyKey.toString());
            log.debug(type.name());
          } else {
            people += peopleCost;
          }
        }
      }
    }
    village.setPopulation(people);
  }
}
