package com.castlewars.service.impl;

import com.castlewars.repository.UserRoleRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Michał Daniel (michal.mateusz.daniel@gmail.com)
 */
@Slf4j
@Transactional
@Service
public class UserRoleServiceImpl {

  @Autowired
  private UserRoleRepository roleRepository;

  @Transactional
  public void setUserPrivilages(Long userId) {
    roleRepository.setUserPrivilages(userId);
  }
}
