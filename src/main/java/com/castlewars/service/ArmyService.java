package com.castlewars.service;

import com.castlewars.entity.military.Army;
import com.castlewars.entity.military.RecruitmentQueue;
import com.castlewars.repository.ArmyRepository;
import com.castlewars.repository.RecruitmentQueueRepository;
import com.castlewars.units.model.UnitType;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Michał
 */
@Slf4j
@Service
public class ArmyService {

  @Autowired
  private ArmyRepository armyRepository;
  @Autowired
  private RecruitmentQueueRepository recruitmentRepository;

  public void createEmptyArmyForNewVillage(Long villageId) {
    Army army = new Army();
    army.setVillageId(villageId);
    army.set(UnitType.SPEAR_FIGHTER, 0L);
    army.set(UnitType.SWORDSMAN, 0L);
    army.set(UnitType.ARCHER, 0L);
    army.set(UnitType.CAVALARY, 0L);
    armyRepository.save(army);

    log.debug("Create army for village with ID: {}.", villageId);
  }

  public Optional<Army> getArmyForVillage(Long villageId) {

    Optional<Army> armyForVillage = armyRepository.getArmyForVillage(villageId);

    List<RecruitmentQueue> recruitmentQueues = recruitmentRepository
            .getAllRecruitmentQueueForVillage(villageId);
    if (!recruitmentQueues.isEmpty()) {

      List<RecruitmentQueue> filtered = recruitmentQueues
              .stream()
              .filter(r -> Objects.equals(r.getStartRecruit(), Boolean.TRUE)
              && Objects.equals(r.getEndRecruit(), Boolean.FALSE))
              .collect(Collectors.toList());

    }

    return armyForVillage;
  }

  public void update(Army army) {
    armyRepository.updateArmy(army.getVillageId(),
            army.getSpearFighter(),
            army.getSwordsman(),
            army.getArcher(),
            army.getCavalary());

  }
}
