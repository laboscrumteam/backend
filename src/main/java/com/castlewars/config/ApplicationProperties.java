package com.castlewars.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Michał Daniel (michal.mateusz.daniel@gmail.com)
 */
@Data
@ConfigurationProperties("app")
@Configuration
public class ApplicationProperties {

  private Auth auth;

  private Endpoint endpoint;

  @Data
  public static class Auth {

    private String resourceId;

    private String kfName;

    private String ksPass;

    private Integer defaultAccessTokenTimeout;

    private Integer defaultRefreshTokenTimeout;

    private Integer failedLoginAttemptAccountLockTimeout;

    private Integer maxFailedLoginAttemptsForAccountLock;

  }

  @Data
  public static class Endpoint {

    private String html;

    private String testEndpoint;

    private String register;
    
    private String village;
    
    private String user;
    
    private String buildinginfo;

    private String buildings;
    
    private String army;
    
    private String map;

  }
}
