package com.castlewars.config;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

/**
 *
 * Created by Michał Daniel (michal.mateusz.daniel@gmail.com)
 */
@Configuration
public class RestConfiguration {

  @Bean
  public CorsFilter corsFilter() {
    UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
    CorsConfiguration config = new CorsConfiguration().applyPermitDefaultValues();
    source.registerCorsConfiguration("/**", config);
    //FilterRegistrationBean bean = new FilterRegistrationBean(new CorsFilter(source));
    //bean.setOrder(0);
    CorsFilter corsFilter = new CorsFilter(source);
    return corsFilter;
  }
}
