package com.castlewars.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;

/**
 * Created by Michał Daniel (michal.mateusz.daniel@gmail.com)
 */
@Configuration
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

  @Autowired
  private DefaultTokenServices tokenServices;

  @Autowired
  private TokenStore tokenStore;

  @Autowired
  private ApplicationProperties applicationProperties;

  @Override
  public void configure(ResourceServerSecurityConfigurer configurer) {
    configurer
            .resourceId(applicationProperties.getAuth().getResourceId())
            .tokenServices(tokenServices)
            .tokenStore(tokenStore);
  }

  @Override
  public void configure(HttpSecurity http) throws Exception {
    http
            .cors().and()
            .csrf().disable()
            .authorizeRequests()
            .antMatchers(applicationProperties.getEndpoint().getTestEndpoint()).hasAuthority("user")
            .antMatchers(applicationProperties.getEndpoint().getVillage()).hasAuthority("user")
            .antMatchers(applicationProperties.getEndpoint().getUser()).hasAuthority("user")
            .antMatchers(applicationProperties.getEndpoint().getBuildinginfo()).hasAuthority("user")
            .antMatchers(applicationProperties.getEndpoint().getBuildings()).hasAuthority("user")
            .antMatchers(applicationProperties.getEndpoint().getArmy()).hasAuthority("user")
            .antMatchers(applicationProperties.getEndpoint().getMap()).hasAuthority("user")
            .antMatchers(applicationProperties.getEndpoint().getHtml()).permitAll()
            .antMatchers(applicationProperties.getEndpoint().getRegister()).permitAll()
            .antMatchers("/**").denyAll();
  }
}
