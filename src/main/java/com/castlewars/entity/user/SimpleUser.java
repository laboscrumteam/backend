package com.castlewars.entity.user;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import javax.persistence.Entity;
import javax.persistence.Id;
import lombok.Data;

/**
 * Create by Michał Daniel (michal.mateusz.daniel@gmail.com)
 */
@Data
@Entity
public class SimpleUser {

  @Id
  private long id;

  private String name;

  public SimpleUser(User u) {
    this.id = u.getId();
    this.name = u.getName();
  }

  @JsonInclude(Include.NON_NULL)
  private Long points;
  
  

}
