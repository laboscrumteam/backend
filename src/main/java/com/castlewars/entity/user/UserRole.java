package com.castlewars.entity.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Michał Daniel (michal.mateusz.daniel@gmail.com)
 */
@Getter
@Setter
@Entity
@Table(catalog = "castlewars", name = "user_role")
public class UserRole {

  @Id
  @Column(name = "role_id", nullable = false)
  private Long role_id;

  
  @Column(name = "user_id", nullable = false)
  private Long user_id;

}
