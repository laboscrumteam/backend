package com.castlewars.entity.user;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * Created by Michał Daniel (michal.mateusz.daniel@gmail.com)
 */

//TODO: work in progress, not to use now. 

@Deprecated
@Getter
@Setter
@Entity
@Table(catalog = "castlewars", name = "user")
public class UserInfoAuthorized{

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
 
  private String name;

}
