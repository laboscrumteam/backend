package com.castlewars.entity.village;

import com.castlewars.buildings.model.enums.BuildingType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

/**
 * Create by Michał Daniel (michal.daniel@makolab.com)
 */
@Getter
@Setter
@Entity
@Table(catalog = "castlewars", name = "village")
public class Village {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "owner_id")
  private Long ownerId;

  private String name;

  private Long headquarters;

  @Column(name = "central_square")
  private Long centralSquare;

  private Long barracks;

  @Column(name = "timber_camp")
  private Long timberCamp;

  @Column(name = "clay_pit")
  private Long clayPit;

  @Column(name = "iron_mine")
  private Long ironMine;

  private Long warehouse;

  private Long wall;

  private Long farm;

  private Long points;

  @Column(name = "location_x")
  private Long locationX;

  @Column(name = "location_y")
  private Long locationY;

  @Column(name = "last_timestamp")
  private long lastTimestamp;

  private long wood;

  private long clay;

  private long iron;

  private long loyalty;
  
  private long population;

  public Long get(BuildingType type) {
    switch (type) {
      case BARRACKS:
        return this.barracks;
      case CLAY_PIT:
        return this.clayPit;
      case CENTRAL_SQUARE:
        return this.centralSquare;
      case FARM:
        return this.farm;
      case HEADQUARTERS:
        return this.headquarters;
      case IRON_MINE:
        return this.ironMine;
      case TIMBER_CAMP:
        return this.timberCamp;
      case WALL:
        return this.wall;
      case WAREHOUSE:
        return this.warehouse;
      default:
        return 0L;
    }
  }
    public void set(BuildingType type, Long val) {
    switch (type) {
      case BARRACKS:
        this.barracks = val;
        break;
      case CLAY_PIT:
        this.clayPit = val;
        break;
      case CENTRAL_SQUARE:
        this.centralSquare= val;
        break;
      case FARM:
        this.farm= val;
        break;
      case HEADQUARTERS:
        this.headquarters= val;
        break;
      case IRON_MINE:
        this.ironMine= val;
        break;
      case TIMBER_CAMP:
        this.timberCamp= val;
        break;
      case WALL:
        this.wall= val;
        break;
      case WAREHOUSE:
        this.warehouse= val;
        break;
      default:
        
    }
  }
}
