package com.castlewars.entity.village.exceptions;

/**
 *
 * Created by Michał Daniel (michal.mateusz.daniel@gmail.com)
 */
public class VillageNotFoundException extends Exception {

  public VillageNotFoundException(String message) {
    super(message);
  }

}
