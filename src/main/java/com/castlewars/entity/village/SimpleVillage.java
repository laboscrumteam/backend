package com.castlewars.entity.village;

import com.castlewars.entity.auth.Role;
import com.castlewars.entity.user.SimpleUser;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

/**
 * Create by Michał Daniel (michal.daniel@makolab.com)
 */
@Getter
@Setter
@Entity
@Table(catalog = "castlewars", name = "village")
public class SimpleVillage {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "owner_id")
  private Long ownerId;
  
  private String name;
  
  private String ownerName;

  private Long points;

  @Column(name = "location_x")
  private Long locationX;

  @Column(name = "location_y")
  private Long locationY;

/*  @ManyToMany(fetch = FetchType.LAZY)
  @JoinTable(catalog = "castlewars", name = "user",
          joinColumns = @JoinColumn(name = "owner_id", referencedColumnName = "id"))
  private SimpleUser user;  */

  public SimpleVillage(Village v, String ownerName) {
    this.id = v.getId();
    this.ownerId = v.getOwnerId();
    this.name = v.getName();
    this.locationX = v.getLocationX();
    this.locationY = v.getLocationY();
    this.points = v.getPoints();
    this.ownerName = ownerName;
    
  }
  
  
  
}
