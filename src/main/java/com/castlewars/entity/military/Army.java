package com.castlewars.entity.military;

import com.castlewars.units.model.UnitType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

/**
 * @author 
 */
@Getter
@Setter
@Entity
@Table(catalog = "castlewars", name = "army")
public class Army {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "village_id")
    private Long villageId;
    @Column(name = "spear_fighter")
    private Long spearFighter;
    private Long swordsman;
    private Long archer;
    private Long cavalary;

    public void set(UnitType ut, Long val) {
        switch(ut){
            case SPEAR_FIGHTER:
                this.spearFighter = val;
                break;
            case SWORDSMAN:
                this.swordsman = val;
                break;
            case ARCHER: 
                this.archer = val;
                break;
            case CAVALARY:
                this.cavalary = val;
                break;
        }
    }

    public Long get(UnitType ut) {
        switch(ut){
            case SPEAR_FIGHTER:
                return this.spearFighter;
            case SWORDSMAN:
                return this.swordsman;
            case ARCHER: 
                 return this.archer;
            case CAVALARY:
                return this.cavalary;
        }
        return 0L;
    }

}
