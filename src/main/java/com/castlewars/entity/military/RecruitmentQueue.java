package com.castlewars.entity.military;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Michał
 */
@Getter
@Setter
@Entity
@Table(catalog = "castlewars", name = "recruitment_queue")
public class RecruitmentQueue {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "village_id")
    private Long villageId;
    @Column(name = "start_timestamp")
    private Long startTimestamp;
    @Column(name = "end_recruit")
    private Boolean endRecruit;
    @Column(name = "start_recruit")
    private Boolean startRecruit;
    @Column(name = "recruit_value")
    private Long recruitValue;
    @Column(name = "recruit_now")
    private Long recruitNow;
    @Column(name = "unit_id")
    private String unitId;
    @Column(name = "last_update")
    private Long lastUpdate;
}
