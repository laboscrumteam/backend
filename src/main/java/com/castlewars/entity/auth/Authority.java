package com.castlewars.entity.auth;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;

/**
 * Created by Michał Daniel (michal.mateusz.daniel@gmail.com)
 */
@Getter
@Setter
@Entity
@Table(catalog = "castlewars", name = "authority")
public class Authority implements GrantedAuthority {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private String name;

  private String code;

  public Authority(String code) {
    this.code = code;
  }

  public Authority() {

  }

  @Override
  public String getAuthority() {
    return code;
  }

}
