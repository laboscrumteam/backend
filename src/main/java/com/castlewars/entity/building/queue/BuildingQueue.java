package com.castlewars.entity.building.queue;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Michał Daniel (michal.mateusz.daniel@gmail.com)
 */

@Getter
@Setter
@Entity
@Table(catalog = "castlewars", name = "buildings_queue")
public class BuildingQueue {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  
  @Column(name = "village_id")
  private Long villageId;
  
  @Column(name = "building_id")
  private String buildingId;
  
  @Column(name = "start_timestamp")
  private Long startTimestamp;
  
  @Column(name = "end_timestamp")
  private Long endTimestamp;
  
  private boolean received;
  
  @Column(name = "next_lvl")
  private Long nextLvl;
  
}
