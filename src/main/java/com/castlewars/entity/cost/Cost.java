package com.castlewars.entity.cost;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Cost {
    private Long wood;
    private Long clay;
    private Long iron;
    private Long people;
}
