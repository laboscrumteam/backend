package com.castlewars.validate.exceptions;

/**
 *
 * Created by Michał Daniel (michal.mateusz.daniel@gmail.com)
 */
public class InvalidEmailException extends ValidationException{

    public InvalidEmailException(String message) {
        super(message);
    }
}
