package com.castlewars.validate.exceptions;

/**
 *
 * Created by Michał Daniel (michal.mateusz.daniel@gmail.com)
 */
public class InvalidPasswordException extends ValidationException{

    public InvalidPasswordException(String message) {
        super(message);
    }

}
