package com.castlewars.validate.exceptions;

/**
 *
 * Created by Michał Daniel (michal.mateusz.daniel@gmail.com)
 */
public class ExceptionMsg {

    public static final String NICKNAME_LETTERS_NUMBERS_ONLY = "Use only letters and numbers.";
    public static final String NICKNAME_LENGTH = "Nickname should be less than 20 and more than 5 characters in length.";
    public static final String NICKNAME_USED = "Nickname is currently used.";

    public static final String PASSWORD_LENGTH = "Password should be less than 35 and more than 8 characters in length.";
    public static final String PASSWORD_THE_SAME = "Password Should not be same as user name";
    public static final String PASSWORD_UPPER_CASE = "Password should contain atleast one upper case alphabet";
    public static final String PASSWORD_LOWER_CASE = "Password should contain atleast one lower case alphabet";
    public static final String PASSWORD_NUMBER = "Password should contain atleast one number.";

    public static final String EMAIL_INVALID = "Invalid email.";
    public static final String EMAIL_USED = "Email is currently used.";

}
