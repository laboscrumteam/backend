package com.castlewars.validate.exceptions;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Michał Daniel (michal.mateusz.daniel@gmail.com)
 */
public class ValidationException extends Exception{

    public ValidationException(String message) {
        super(message);
    }
    
    public Map convertToObject(){
       Map map = new HashMap<String,String>();
       map.put("type", "ValidationException");
       map.put("spec", this.getClass().getSimpleName());
       map.put("message", super.getMessage());
       return map;
    }
}
