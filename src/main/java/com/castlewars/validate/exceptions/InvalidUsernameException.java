package com.castlewars.validate.exceptions;

/**
 *
 * Created by Michał Daniel (michal.mateusz.daniel@gmail.com)
 */
public class InvalidUsernameException extends ValidationException{

    public InvalidUsernameException(String message) {
        super(message);
    }

}
