package com.castlewars.validate;

import com.castlewars.entity.user.User;
import com.castlewars.repository.UserRepository;
import com.castlewars.validate.exceptions.ExceptionMsg;
import com.castlewars.validate.exceptions.InvalidEmailException;
import com.castlewars.validate.exceptions.InvalidUsernameException;
import com.castlewars.validate.exceptions.InvalidPasswordException;
import com.castlewars.validate.exceptions.ValidationException;
import java.util.Optional;
import java.util.regex.Pattern;
import org.apache.commons.validator.EmailValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * Created by Michał Daniel (michal.mateusz.daniel@gmail.com)
 */
@Component
public class UserValidator {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserValidator.class);
    private static final Pattern PATTERN_NICKNAME_VALIDATION = Pattern.compile("[A-Za-z0-9_]+");
    private static final EmailValidator EMAIL_VALIDATOR = EmailValidator.getInstance();
    

    @Autowired
    private UserRepository userRepository;

    public boolean isValid(User user) throws ValidationException {

        return passwordValidation(user.getName(), user.getPassword())
                && nameValidation(user.getName())
                && emailValidation(user.getUsername());
    }

    private boolean nameValidation(String nickname) throws InvalidUsernameException {

        if (!((nickname != null) && PATTERN_NICKNAME_VALIDATION.matcher(nickname).matches())) {
            LOGGER.debug(ExceptionMsg.NICKNAME_LETTERS_NUMBERS_ONLY);
            throw new InvalidUsernameException(ExceptionMsg.NICKNAME_LETTERS_NUMBERS_ONLY);
        }
        if (nickname.length() > 20 || nickname.length() < 5) {
            LOGGER.debug(ExceptionMsg.NICKNAME_LENGTH);
            throw new InvalidUsernameException(ExceptionMsg.NICKNAME_LENGTH);
        }
        Optional<User> checkNickname = userRepository.checkName(nickname);
        if (checkNickname.isPresent()) {
            LOGGER.debug(ExceptionMsg.NICKNAME_USED);
            throw new InvalidUsernameException(ExceptionMsg.NICKNAME_USED);
        }

        return true;
    }

    private boolean passwordValidation(String userName, String password) throws InvalidPasswordException {
        if (password.length() > 35 || password.length() < 8) {
            LOGGER.debug(ExceptionMsg.PASSWORD_LENGTH);
            throw new InvalidPasswordException(ExceptionMsg.PASSWORD_LENGTH);
        }
        if (password.contains(userName)) {
            LOGGER.debug(ExceptionMsg.PASSWORD_THE_SAME);
            throw new InvalidPasswordException(ExceptionMsg.PASSWORD_THE_SAME);
        }
        String upperCaseChars = "(.*[A-Z].*)";
        if (!password.matches(upperCaseChars)) {
            LOGGER.debug(ExceptionMsg.PASSWORD_UPPER_CASE);
            throw new InvalidPasswordException(ExceptionMsg.PASSWORD_UPPER_CASE);
        }
        String lowerCaseChars = "(.*[a-z].*)";
        if (!password.matches(lowerCaseChars)) {
            LOGGER.debug(ExceptionMsg.PASSWORD_LOWER_CASE);
            throw new InvalidPasswordException(ExceptionMsg.PASSWORD_LOWER_CASE);
        }
        String numbers = "(.*[0-9].*)";
        if (!password.matches(numbers)) {
            LOGGER.debug(ExceptionMsg.PASSWORD_NUMBER);
            throw new InvalidPasswordException(ExceptionMsg.PASSWORD_NUMBER);
        }
        /*String specialChars = "(.*[,~,!,@,#,$,%,^,&,*,(,),-,_,=,+,[,{,],},|,;,:,<,>,/,?].*$)";
        if (!password.matches(specialChars)) {
            throw new InvalidPasswordException("Password should contain atleast one special character");
        }*/
        return true;
    }

    private boolean emailValidation(String email) throws InvalidEmailException {
        boolean valid = EMAIL_VALIDATOR.isValid(email);
        if (valid == false) {
            LOGGER.debug(ExceptionMsg.EMAIL_INVALID);
            throw new InvalidEmailException(ExceptionMsg.EMAIL_INVALID);
        }
        Optional<User> checkEmail = userRepository.checkEmail(email);
        if (checkEmail.isPresent()) {
            LOGGER.debug(ExceptionMsg.EMAIL_USED);
            throw new InvalidEmailException(ExceptionMsg.EMAIL_USED);
        }
        return valid;
    }
}
