package com.castlewars.units;

import com.castlewars.units.model.Unit;
import com.castlewars.units.model.UnitProperty;
import com.castlewars.units.model.UnitType;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * Created by Michał Daniel (michal.mateusz.daniel@gmail.com)
 */
@Slf4j
@Component
public class UnitsProperties {

    private Map<UnitType, Unit> unitsProperties;

    @PostConstruct
    public void init() {
        log.info("Prepare units data.");
        unitsProperties = new HashMap<>();

        Unit spearFighter = UnitsDataGenerator.generateSpearFighterData();
        unitsProperties.put(UnitType.SPEAR_FIGHTER, spearFighter);

        Unit swordsman = UnitsDataGenerator.generateSwordsmanData();
        unitsProperties.put(UnitType.SWORDSMAN, swordsman);

        Unit archer = UnitsDataGenerator.generateArcherData();
        unitsProperties.put(UnitType.ARCHER, archer);

        Unit cavalery = UnitsDataGenerator.generateCavalaryData();
        unitsProperties.put(UnitType.CAVALARY, cavalery);

        
    }

    public Long getUnitProperty(UnitType ut, UnitProperty up) {
        return unitsProperties.get(ut).getProperty(up);
    }
}
