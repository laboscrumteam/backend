package com.castlewars.units.model;

/**
 * Created by Michał Daniel (michal.mateusz.daniel@gmail.com)
 */
public enum UnitType {
  SPEAR_FIGHTER,
  SWORDSMAN,
  ARCHER,
  CAVALARY
}
