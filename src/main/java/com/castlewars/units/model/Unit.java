package com.castlewars.units.model;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Michał
 */
public class Unit {

    private Map<UnitProperty, Long> properties;

    public Unit() {
        properties = new HashMap<>();
    }
    public Long getProperty(UnitProperty up) {
        return properties.get(up);
    }
    
    public void addProperty(UnitProperty up, Long value){
        properties.put(up, value);
    }

}
