package com.castlewars.units.model;

/**
 * Created by Michał Daniel (michal.mateusz.daniel@gmail.com)
 */
public enum UnitProperty {
  WOOD_COST,
  CLAY_COST,
  IRON_COST,
  PEOPLE_COST,
  OFFENSIVE_STRENGTH,
  DEFENSE_STRENGTH,
  SPEED,
  HAUL,
  MIN_BARRACKS_LVL,
  RECRUIT_TIME
}
