package com.castlewars.units;

import com.castlewars.units.model.Unit;
import com.castlewars.units.model.UnitProperty;

/**
 * Created by Michal Daniel (michal.mateusz.daniel@gmail.com)
 */
public class UnitsDataGenerator {
    
    protected static Unit generateCavalaryData() {
        Unit cavalery = new Unit();
        cavalery.addProperty(UnitProperty.WOOD_COST, 250L);
        cavalery.addProperty(UnitProperty.CLAY_COST, 225L);
        cavalery.addProperty(UnitProperty.IRON_COST, 250L);
        cavalery.addProperty(UnitProperty.DEFENSE_STRENGTH, 5L);
        cavalery.addProperty(UnitProperty.OFFENSIVE_STRENGTH, 110L);
        cavalery.addProperty(UnitProperty.SPEED, 80L);
        cavalery.addProperty(UnitProperty.HAUL, 100L);
        cavalery.addProperty(UnitProperty.PEOPLE_COST, 3L);
        cavalery.addProperty(UnitProperty.MIN_BARRACKS_LVL, 15L);
        cavalery.addProperty(UnitProperty.RECRUIT_TIME, 600L);
        return cavalery;
    }

    protected static Unit generateArcherData() {
        Unit archer = new Unit();
        archer.addProperty(UnitProperty.WOOD_COST, 70L);
        archer.addProperty(UnitProperty.CLAY_COST, 30L);
        archer.addProperty(UnitProperty.IRON_COST, 40L);
        archer.addProperty(UnitProperty.DEFENSE_STRENGTH, 10L);
        archer.addProperty(UnitProperty.OFFENSIVE_STRENGTH, 40L);
        archer.addProperty(UnitProperty.SPEED, 130L);
        archer.addProperty(UnitProperty.HAUL, 10L);
        archer.addProperty(UnitProperty.PEOPLE_COST, 1L);
        archer.addProperty(UnitProperty.MIN_BARRACKS_LVL, 10L);
        archer.addProperty(UnitProperty.RECRUIT_TIME, 260L);
        return archer;
    }

    protected static Unit generateSwordsmanData() {
        Unit swordsman = new Unit();
        swordsman.addProperty(UnitProperty.WOOD_COST, 40L);
        swordsman.addProperty(UnitProperty.CLAY_COST, 45L);
        swordsman.addProperty(UnitProperty.IRON_COST, 100L);
        swordsman.addProperty(UnitProperty.DEFENSE_STRENGTH, 50L);
        swordsman.addProperty(UnitProperty.OFFENSIVE_STRENGTH, 25L);
        swordsman.addProperty(UnitProperty.SPEED, 120L);
        swordsman.addProperty(UnitProperty.HAUL, 15L);
        swordsman.addProperty(UnitProperty.PEOPLE_COST, 1L);
        swordsman.addProperty(UnitProperty.MIN_BARRACKS_LVL, 5L);
        swordsman.addProperty(UnitProperty.RECRUIT_TIME, 230L);
        return swordsman;
    }

    protected static Unit generateSpearFighterData() {
        Unit spearFighter = new Unit();
        spearFighter.addProperty(UnitProperty.WOOD_COST, 50L);
        spearFighter.addProperty(UnitProperty.CLAY_COST, 30L);
        spearFighter.addProperty(UnitProperty.IRON_COST, 10L);
        spearFighter.addProperty(UnitProperty.DEFENSE_STRENGTH, 25L);
        spearFighter.addProperty(UnitProperty.OFFENSIVE_STRENGTH, 10L);
        spearFighter.addProperty(UnitProperty.SPEED, 150L);
        spearFighter.addProperty(UnitProperty.HAUL, 25L);
        spearFighter.addProperty(UnitProperty.PEOPLE_COST, 1L);
        spearFighter.addProperty(UnitProperty.MIN_BARRACKS_LVL, 1L);
        spearFighter.addProperty(UnitProperty.RECRUIT_TIME, 175L);
        return spearFighter;
    }

}
