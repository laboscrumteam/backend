package com.castlewars.buildings;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Michał Daniel (michal.mateusz.daniel@gmail.com)
 */
@Slf4j
@Data
@ConfigurationProperties("building")
@Configuration
public class BuildingStartProperties {

  private InitialData headquadrates;
  private InitialData barracks;
  private InitialData timbercamp;
  private InitialData claypit;
  private InitialData ironmine;
  private InitialData centralsquare;
  private InitialData warehouse;
  private InitialData wall;
  private InitialData farm;

  @Data
  public static class InitialData {

    private Long wood;
    private Long clay;
    private Long iron;
    private Long people;
    private Long time;
    private Long bonus;
    private Long maxlvl;
    private Long startlvl;
  }

}
