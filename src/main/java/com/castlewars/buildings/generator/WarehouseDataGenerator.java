package com.castlewars.buildings.generator;

import com.castlewars.buildings.BuildingStartProperties;
import com.castlewars.buildings.model.Building;
import com.castlewars.buildings.model.Pair;
import com.castlewars.buildings.model.enums.BuildingProperty;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Michał Daniel (michal.mateusz.daniel@gmail.com)
 */
@Configuration
public class WarehouseDataGenerator extends DataGeneratorAbstract implements DataGenerator {

  @Autowired
  private BuildingStartProperties initialData;

  @Override
  public Building generateData() {
    Building building = new Building();
    building.setMaxLvl(initialData.getWarehouse().getMaxlvl());
    building.setStartLvl(initialData.getWarehouse().getStartlvl());

    Map<Pair<Long, BuildingProperty>, Long> prop = new HashMap<>();

    basicData(prop);

    expandData(building, prop);

    building.setProperties(prop);

    return building;
  }

  private void expandData(Building building, Map<Pair<Long, BuildingProperty>, Long> prop) {
    for (int lvli = 2; lvli <= building.getMaxLvl(); lvli++) {
      Long lvl = new Integer(lvli).longValue();
      Long nextWood = new Double( Math.ceil(prop.get(new Pair<>(lvl - 1, BuildingProperty.WOOD_COST))
              + initialData.getWarehouse().getWood() / 1 + lvl * lvl)).longValue();
      prop.put(new Pair<>(lvl, BuildingProperty.WOOD_COST), nextWood);

      Long nextClay = new Double( Math.ceil(prop.get(new Pair<>(lvl - 1, BuildingProperty.CLAY_COST))
              + initialData.getWarehouse().getClay() / 3 + lvl * lvl)).longValue();
      prop.put(new Pair<>(lvl, BuildingProperty.CLAY_COST), nextClay);

      Long nextIron = new Double( Math.ceil(prop.get(new Pair<>(lvl - 1, BuildingProperty.IRON_COST))
              + initialData.getWarehouse().getIron() / 2 + lvl * lvl)).longValue();
      prop.put(new Pair<>(lvl, BuildingProperty.IRON_COST), nextIron);

      Long nextPeople = lvl*2;
      prop.put(new Pair<>(lvl, BuildingProperty.PEOPLE_COST), nextPeople);

      Long nextBuildTime = new Double( Math.ceil(prop.get(new Pair<>(lvl - 1, BuildingProperty.BUILD_TIME)) + lvl * 2)).longValue();
      prop.put(new Pair<>(lvl, BuildingProperty.BUILD_TIME), nextBuildTime);

      Long nextBonus = calculateNextBonus(prop.get(new Pair<>(lvl - 1, BuildingProperty.BONUS)), 256 * lvl);
      prop.put(new Pair<>(lvl, BuildingProperty.BONUS), nextBonus);

      Long nextPoints = calculatePoints(building.getMaxLvl(), lvli);
      prop.put(new Pair<>(lvl, BuildingProperty.POINTS_PER_LVL), nextPoints);

    }
  }

  private void basicData(Map<Pair<Long, BuildingProperty>, Long> properties) {
    properties.put(new Pair<>(1L, BuildingProperty.WOOD_COST), initialData.getWarehouse().getWood());
    properties.put(new Pair<>(1L, BuildingProperty.CLAY_COST), initialData.getWarehouse().getClay());
    properties.put(new Pair<>(1L, BuildingProperty.IRON_COST), initialData.getWarehouse().getIron());
    properties.put(new Pair<>(1l, BuildingProperty.PEOPLE_COST), initialData.getWarehouse().getPeople());
    properties.put(new Pair<>(1L, BuildingProperty.BUILD_TIME), initialData.getWarehouse().getTime());
    properties.put(new Pair<>(1L, BuildingProperty.BONUS), initialData.getWarehouse().getBonus());
    properties.put(new Pair<>(1L, BuildingProperty.POINTS_PER_LVL),
            calculatePoints(initialData.getWarehouse().getMaxlvl(), 1));
  }

}
