package com.castlewars.buildings.generator;

import com.castlewars.buildings.BuildingStartProperties;
import com.castlewars.buildings.model.Building;
import com.castlewars.buildings.model.Pair;
import com.castlewars.buildings.model.enums.BuildingProperty;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Michał Daniel (michal.mateusz.daniel@gmail.com)
 */
@Configuration
public class CentralSquareGenerator extends DataGeneratorAbstract implements DataGenerator {

  @Autowired
  private BuildingStartProperties initialData;

  @Override
  public Building generateData() {
    Building building = new Building();
    building.setMaxLvl(initialData.getCentralsquare().getMaxlvl());
    building.setStartLvl(initialData.getCentralsquare().getStartlvl());

    Map<Pair<Long, BuildingProperty>, Long> prop = new HashMap<>();

    basicData(prop);

    building.setProperties(prop);

    return building;
  }

  private void basicData(Map<Pair<Long, BuildingProperty>, Long> properties) {
    properties.put(new Pair<>(1L, BuildingProperty.WOOD_COST), initialData.getCentralsquare().getWood());
    properties.put(new Pair<>(1L, BuildingProperty.CLAY_COST), initialData.getCentralsquare().getClay());
    properties.put(new Pair<>(1L, BuildingProperty.IRON_COST), initialData.getCentralsquare().getIron());
    properties.put(new Pair<>(1L, BuildingProperty.PEOPLE_COST), initialData.getCentralsquare().getPeople());
    properties.put(new Pair<>(1L, BuildingProperty.BUILD_TIME), initialData.getCentralsquare().getTime());
    properties.put(new Pair<>(1L, BuildingProperty.BONUS), initialData.getCentralsquare().getBonus());
    properties.put(new Pair<>(1L, BuildingProperty.POINTS_PER_LVL),
            calculatePoints(initialData.getCentralsquare().getMaxlvl(), 1));
  }

}
