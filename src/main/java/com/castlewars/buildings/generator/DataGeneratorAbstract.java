package com.castlewars.buildings.generator;

/**
 * Created by Michał Daniel (michal.mateusz.daniel@gmail.com)
 */
public abstract class DataGeneratorAbstract {

  public Long calculatePoints(Long maxLvl, int currentLvl) {
    return new Double(Math.ceil(((100 - maxLvl) * currentLvl) / 10)).longValue();
  }
  
  public Long calculateNextBonus(Long currentBonus, Long increment){
    return new Double(Math.ceil(currentBonus + increment)).longValue();
  }
}
