package com.castlewars.buildings.generator;

import com.castlewars.buildings.model.Building;

/**
 *
 * Created by Michał Daniel (michal.mateusz.daniel@gmail.com)
 */
public interface DataGenerator {
  Building generateData();
  
}
