package com.castlewars.buildings;

import com.castlewars.buildings.generator.BarracksDataGenerator;
import com.castlewars.buildings.generator.CentralSquareGenerator;
import com.castlewars.buildings.generator.ClayPitDataGenerator;
import com.castlewars.buildings.generator.FarmGenerator;
import com.castlewars.buildings.generator.HeadquadratesDataGenerator;
import com.castlewars.buildings.generator.IronMineDataGenerator;
import com.castlewars.buildings.generator.TimberCampDataGenerator;
import com.castlewars.buildings.generator.WallDataGenerator;
import com.castlewars.buildings.generator.WarehouseDataGenerator;
import com.castlewars.buildings.model.Building;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Michał Daniel (michal.mateusz.daniel@gmail.com)
 */
@Configuration
public class BuildingDataGenerator {

  @Autowired
  private HeadquadratesDataGenerator headquadratesDataGenerator;
  @Autowired
  private BarracksDataGenerator barracksDataGenerator;
  @Autowired
  private CentralSquareGenerator centralSquareGenerator;
  @Autowired
  private ClayPitDataGenerator clayPitDataGenerator;
  @Autowired
  private FarmGenerator farmGenerator;
  @Autowired
  private IronMineDataGenerator ironMineDataGenerator;
  @Autowired
  private TimberCampDataGenerator timberCampDataGenerator;
  @Autowired
  private WallDataGenerator wallDataGenerator;
  @Autowired
  private WarehouseDataGenerator warehouseDataGenerator;
  

  public Building headquadratesData() {

    return headquadratesDataGenerator.generateData();
  }

  public Building barracksData() {

    return barracksDataGenerator.generateData();
  }
  
  public Building centralSquareData(){
    return centralSquareGenerator.generateData();
  }
  
  public Building clayPitData(){
    return clayPitDataGenerator.generateData();
  }
  
  public Building farmData(){
    return farmGenerator.generateData();
  }
  
  public Building ironMineData() {
    return ironMineDataGenerator.generateData();
  }
  
  public Building timberCampData() {
    return timberCampDataGenerator.generateData();
  }
  
  public Building wallData() {
    return wallDataGenerator.generateData();
  }
  
  public Building warehouseData() {
    return warehouseDataGenerator.generateData();
  }
}
