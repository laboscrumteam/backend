package com.castlewars.buildings;

import com.castlewars.buildings.model.Building;
import com.castlewars.buildings.model.enums.BuildingType;
import javax.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * Created by Michał Daniel (michal.mateusz.daniel@gmail.com)
 */
@Slf4j
@Component
public class BuildingsProperties {

  @Getter
  private Map<BuildingType, Building> buildings;
  @Autowired
  private BuildingDataGenerator dataGenerator;

  @PostConstruct
  public void init() {
    log.info("Prepare buildings data.");
    buildings = new HashMap<>();
    buildings.put(BuildingType.HEADQUARTERS, dataGenerator.headquadratesData());
    buildings.put(BuildingType.BARRACKS, dataGenerator.barracksData());
    buildings.put(BuildingType.CENTRAL_SQUARE, dataGenerator.centralSquareData());
    buildings.put(BuildingType.CLAY_PIT, dataGenerator.clayPitData());
    buildings.put(BuildingType.FARM, dataGenerator.farmData());
    buildings.put(BuildingType.IRON_MINE, dataGenerator.ironMineData());
    buildings.put(BuildingType.TIMBER_CAMP, dataGenerator.timberCampData());
    buildings.put(BuildingType.WALL, dataGenerator.wallData());
    buildings.put(BuildingType.WAREHOUSE, dataGenerator.warehouseData());
  }
  
  public Building getBuilding(BuildingType type){
    return buildings.get(type);
  }
}
