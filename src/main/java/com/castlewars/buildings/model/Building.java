package com.castlewars.buildings.model;

import com.castlewars.buildings.model.enums.BuildingProperty;
import lombok.Getter;
import lombok.Setter;
import java.util.Map;

/**
 * Created by Michał Daniel (michal.mateusz.daniel@gmail.com)
 */
@Getter
@Setter
public class Building {
  
  private Long maxLvl;
  private Long startLvl;
  private Map<Pair<Long, BuildingProperty>, Long> properties;
  
  public Long getValue(Long level, BuildingProperty property){
    return properties.get(new Pair<>(level, property));
  }
  
}
