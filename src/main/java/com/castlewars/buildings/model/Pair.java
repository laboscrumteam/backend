package com.castlewars.buildings.model;

import java.util.Objects;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Michal Daniel (michal.mateusz.daniel@gmail.com).
 */
@Getter
@Setter
@AllArgsConstructor
public class Pair<T1, T2> {
    private T1 key;
    private T2 value;

  @Override
  public int hashCode() {
    int hash = 7;
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final Pair<?, ?> other = (Pair<?, ?>) obj;
    if (!Objects.equals(this.key, other.key)) {
      return false;
    }
    if (!Objects.equals(this.value, other.value)) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "<" + key + ", " + value + '>';
  }

    
    
}
