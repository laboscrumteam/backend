package com.castlewars.buildings.model.enums;

/**
 * Created by Michał Daniel (michal.mateusz.daniel@gmail.com)
 */
public enum BuildingProperty {
  WOOD_COST,
  CLAY_COST,
  IRON_COST,
  PEOPLE_COST,
  BUILD_TIME,
  BONUS,
  POINTS_PER_LVL
}
