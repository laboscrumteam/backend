package com.castlewars.buildings.model.enums;

/**
 * Created by Michał Daniel (michal.mateusz.daniel@gmail.com)
 */
public enum BuildingType {

  HEADQUARTERS,
  CENTRAL_SQUARE,
  BARRACKS,
  TIMBER_CAMP,
  CLAY_PIT,
  IRON_MINE,
  WAREHOUSE,
  WALL,
  FARM;
}
