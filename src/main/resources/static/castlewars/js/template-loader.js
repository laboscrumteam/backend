/* global container, templateLocation, data */

function parseTemplate(templateLocation, data, container) {
   
    $(container).loadTemplate(templateLocation,data,{async:false});
}

function parseTemplateWithConfig(configParser) {
    parseTemplate(configParser.templateLocation, configParser.data, configParser.container);
}

function parseTemplateWithConfigAppend(configParser){
    parseTemplateAppend(configParser.templateLocation, configParser.data, configParser.container);
}

function parseTemplateAppend(templateLocation, data, container){
        $(container).loadTemplate(templateLocation,data,{append:true, async:false});
}