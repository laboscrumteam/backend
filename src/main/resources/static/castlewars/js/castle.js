$(document).ready(function () {
  testLogged();
  setInterval(updateResources, 60000);
});

const content = {
  NOT_LOGGER: 'not-logged',
  VILLAGE: 'village',
  CASTLE: 'castle'
};

function testLogged() {

  var token = sessionStorage.getItem("acc_token");
  $.ajax({
    url: "api/testAuth",
    xhrFields: {
      withCredentials: true
    },
    headers: {
      'Authorization': `Bearer ` + token
    },
    method: 'GET',
    success: function (data) {
      loadBuildingData();
      showContent(content.VILLAGE);
    },
    error: function () {
      showContent(content.NOT_LOGGER);
      showalert("Błąd logowania", "Zaloguj się ponownie na <a href=\"index.html\"> stronie głównej </a>. ", 'warning');
    }

  });
}

function updateResources() {
  var currentVillageId = sessionStorage.getItem("current_village");
  if (currentVillageId === '' || currentVillageId === null) {
    getVillages();
    return;
  }
  testLogged();
  var token = getAccessToken();
  $.ajax({
    url: "api/village/" + currentVillageId + "/resources",
    xhrFields: {
      withCredentials: true
    },
    headers: {
      'Authorization': `Bearer ` + token,
    },
    method: 'GET',
    success: function (data) {
      $resourceConfigParser = {
        templateLocation: "resources.html",
        data: data,
        container: '#resources'
      };
      parseTemplateWithConfig($resourceConfigParser);
    },
    error: function () {
      showalert("Błąd", "Spróbuj ponownie później.", 'warning');
    }

  });
}


function showContent(contentType) {
  switch (contentType) {
    case content.NOT_LOGGER:
      showContentById("#error-login");
      break;
    case content.VILLAGE:
      showContentById("#village-info");
      loadVillage();
      break;
    case content.CASTLE:
      showContentById("#castle");
      loadCastle();
      break;
  }
}
function showContentById(elementID) {
  $(".content-element").each((index, element) => {
    $(element).addClass("d-none");
  });
  $(elementID).removeClass("d-none");
}

function loadCastle() {
  var currentVillageId = sessionStorage.getItem("current_village");
  if (currentVillageId === '' || currentVillageId === null) {
    getVillages();
    return;
  }
  var token = getAccessToken();
  $.ajax({
    url: "api/bulding/" + currentVillageId,
    xhrFields: {
      withCredentials: true
    },
    headers: {
      'Authorization': `Bearer ` + token
    },
    method: 'GET',
    success: function (data) {
      console.log(data);



    },
    error: function () {
      showalert("Błąd", "Spróbuj ponownie później.", 'warning');
    }
  });


}

function loadVillage() {
  var currentVillageId = sessionStorage.getItem("current_village");
  if (currentVillageId === '' || currentVillageId === null) {
    getVillages();
    return;
  }
  var token = getAccessToken();
  $.ajax({
    url: "api/village/" + currentVillageId,
    xhrFields: {
      withCredentials: true
    },
    headers: {
      'Authorization': `Bearer ` + token,
    },
    method: 'GET',
    success: function (data) {
      console.log(data);
      // headquarters = data[];


      $.each(data, function (index, value) {
        console.log(index + " " + value);
        switch (index) {
          case "headquarters":
          case "barracks":
          case "wall":
          case "warehouse":
          case "farm":
            if (value > 0) {
              key = generateKey(value, "BONUS");
              bonus = getBonus(key, index.toUpperCase());
              //console.log(bonus);
              data[index + "_bonus"] = bonus;
            } else {
              data[index + "_bonus"] = "Nie wybudowano";
            }
            break;
          case "centralSquare":
            if (value > 0) {
              key = generateKey(value, "BONUS");
              bonus = getBonus(key, "CENTRAL_SQUARE");
              //console.log(bonus);
              data[index + "_bonus"] = bonus;
            } else {
              data[index + "_bonus"] = "Nie wybudowano";
            }
            break;
          case "ironMine":
            if (value > 0) {
              key = generateKey(value, "BONUS");
              bonus = getBonus(key, "IRON_MINE");
              //console.log(bonus);
              data[index + "_bonus"] = bonus;
            } else {
              data[index + "_bonus"] = "Nie wybudowano";
            }
            break;
          case "timberCamp":
            if (value > 0) {
              key = generateKey(value, "BONUS");
              bonus = getBonus(key, "TIMBER_CAMP");
              //console.log(bonus);
              data[index + "_bonus"] = bonus;
            } else {
              data[index + "_bonus"] = "Nie wybudowano";
            }
            break;
          case "clayPit":
            if (value > 0) {
              key = generateKey(value, "BONUS");
              bonus = getBonus(key, "CLAY_PIT");
              //console.log(bonus);
              data[index + "_bonus"] = bonus;
            } else {
              data[index + "_bonus"] = "Nie wybudowano";
            }
            break;

        }
      });
      $resourcess = {
        wood: data['wood'],
        iron: data['iron'],
        clay: data['clay'],
        population: data['population']
      };
      console.log(data);
      $configParser = {
        templateLocation: "village-info.html",
        data: data,
        container: '#village-info'
      };

      parseTemplateWithConfig($configParser);
      $resourceConfigParser = {
        templateLocation: "resources.html",
        data: $resourcess,
        container: '#resources'
      };
      parseTemplateWithConfig($resourceConfigParser);
      hideLvl0Buildings();


    },
    error: function () {
      showalert("Błąd", "Spróbuj ponownie później.", 'warning');
    }

  });
}

function getVillages() {
  var token = getAccessToken();
  $.ajax({
    url: "api/village",
    xhrFields: {
      withCredentials: true
    },
    headers: {
      'Authorization': `Bearer ` + token,
    },
    method: 'GET',
    success: function (data) {
      if (data.length > 0) {
        console.log(data);
        var villageId = data[0];
        sessionStorage.setItem("current_village", villageId);
        loadVillage();
      } else {
        showalert("Przykro nam.", "Niestety nie posiadasz żadnej wioski na tym świecie.", 'warning');
      }
    },
    error: function () {
      showalert("Błąd", "Spróbuj ponownie później.", 'warning');
    }

  });
}

function hideLvl0Buildings() {
  $(".building-list").each((index, element) => {
    lvlElement = $(element).find(".building-lvl");
    if (lvlElement.text() === "0") {
      $(element).addClass("d-none");
    }
  });
  ;
}

function getBonus(key, building) {
  var info = JSON.parse(localStorage.getItem("build_info"));
  console.log(info);
  console.log(key);
  console.log(building);
  return info[building].properties[key];
}

function arrayRemove(arr, value) {

  return arr.filter(function (ele) {
    return ele != value;
  });

}

function loadBuildingData() {
  var token = getAccessToken();
  $.ajax({
    url: "api/building/info",
    xhrFields: {
      withCredentials: true
    },
    headers: {
      'Authorization': `Bearer ` + token,
    },
    method: 'GET',
    success: function (data) {
      localStorage.setItem("build_info", JSON.stringify(data));
    },
    error: function () {
      console.error("Can not load buildings details");
    }

  });
}

function generateKey(valA, valB) {
  return "<" + valA + ", " + valB + ">"
}