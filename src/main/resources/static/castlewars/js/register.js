/* global validator */

function register(name, email, password, repassword) {
  var url = "http://localhost:8090/castlewars/api/register?" + "name=" + name + "&email=" + email + "&password=" + password;
  console.log(url);

  let validate = validator.validate(name, email, password, repassword);
  if (!validate) {
    return;
  }

  $.ajax({
    type: "GET",
    url: url,
    success: function (result) {
      showalert("Rejestracja:", "Rejestracja użytkownika " + name + " przebiegła pomyślnie.", 'success');
    },
    error: function (error) {
      switch (error['responseJSON']['spec']) {
        case "InvalidUsernameException":
          showalert("Rejestracja", "Nazwa użytkownika nie spełnia wymagań / podana nazwa jest już zajęta", 'warning');
          return;
        case "InvalidEmailException":
          showalert("Rejestracja", "Niepoprawny e-mail lub email już w użyciu", 'warning');
          return;
        case "InvalidPasswordException":
          showalert("Rejestracja", "Hasło nie spełnia wymagań", 'warning');
          return;
        default:
          showalert("Rejestracja", "Błąd przy rejestracji", 'warning');
          return;
      }


    }
  });
}
