function login(email, password) {

    var url = "http://localhost:8090/oauth/token?grant_type=password&username=" + email + "&password=" + password;
    console.log(url);
    sessionStorage.clear();
    $.ajax({
        xhrFields: {
            withCredentials: true
        },
        headers: {
            'Authorization': 'Basic ' + btoa('WEBSITE_CLIENT:website')
        },
        type: "POST",
        url: url,
        success: function (result) {
            showalert("LOGIN", "Pomyślnie zalogowano użytkownika", 'success');
            /*showalert("Success logged", "warning alert " + result["token_type"], 'warning');
            showalert("Success logged", "info alert " + result["token_type"], 'info');
            showalert("Success logged", "danger alert " + result["token_type"], 'danger');*/
            $acc_token = result["access_token"];
            $ref_token = result["refresh_token"];

            console.log($acc_token);
            sessionStorage.setItem("acc_token", $acc_token);

            getSimpleUserInfo();
            

        },
        error: function (jqXHR, textStatus, errorThrown) {
            var errorType = jqXHR["responseJSON"]["error"];
            console.log(errorType);

            if(errorType === "invalid_grant"){
                showalert("Nieudane logowanie", "Niepoprawne dane, sprawdź i spróbuj ponownie", 'danger');
            }
            else {
                showalert("Nieudane logowanie", "Spróbuj ponownie później", 'warning');
            }

        
        }
    });
}

function getSimpleUserInfo(){
    var token = sessionStorage.getItem("acc_token");
    var urlu = "api/user";
    console.log(token);
    $.ajax({
        url: urlu,
        xhrFields: {
            withCredentials: true
        },
        headers: {
            'Authorization': `Bearer ` + token,
        },
        method: 'GET',
        success: function (data) {
            console.log(data);
            sessionStorage.setItem("user_data",JSON.stringify(data));
            showUserInfo(data["name"]);
        }
    });

}


function testToken() {

    var token = sessionStorage.getItem("acc_token");
    console.log(token);
    $.ajax({
        url: "http://localhost:8090/castlewars/api/testAuth",
        xhrFields: {
            withCredentials: true
        },
        headers: {
            'Authorization': `Bearer ` + token,
        },
        method: 'GET',
        success: function (data) {
            var userData = JSON.parse(sessionStorage.getItem("user_data"));
            showUserInfo(userData["name"]);
        }
        
    });

}

function showUserInfo(email) {
    $("#logged-email").text(email);
    $("#login-info").removeClass("d-none");
    $("#login-form").addClass("d-none");
}

$(document).ready(function() {
    testToken();
 }); 