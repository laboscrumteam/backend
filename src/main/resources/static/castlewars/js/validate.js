var validator = {
  validate: function (name, email, password, repassword) {
    if (!validator.validatePassword(password)) {
      showalert("Rejestracja", "Hasło nie spełnia wymagań", 'warning');
      return false;
    }
    if (!validator.validateName(name)) {
      showalert("Rejestracja", "Nazwa użytkownika nie spełnia wymagań", 'warning');
      return false;
    }
    if (!validator.matchPassword(password, repassword)) {
      showalert("Rejestracja", "Podane hasła nie są indentyczne", 'warning');
      return false;
    }
    if (!validator.validateEmail(email)) {
      showalert("Rejestracja", "Niepoprawny e-mail", 'warning');
      return false;
    }

    return true;
  },
  validateEmail: function (email) {
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
  },
  validatePassword: function (password) {
    // at least one number, one lowercase and one uppercase letter
    // at least six characters
    var re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/;
    return re.test(password);
  },
  validateName: function (name) {
    var re = /^[a-zA-Z0-9\-]+$/;;
    return re.test(name);
  },
  matchPassword: function (password, repassword) {
    if (password === repassword) {
      return true;
    }
    return false;
  }
};
