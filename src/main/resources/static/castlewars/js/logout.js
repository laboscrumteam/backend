function logout(){
    sessionStorage.clear();
    $("#login-info").addClass("d-none");
    $("#login-form").removeClass("d-none");
    window.location.replace("index.html");
}