function showalert(messagePrimary, message, alertType) {

    var alertTemplateLocation = "";

    switch (alertType) {
        case "warning":
            alertTemplateLocation = "warningAlert.html"
            break;
        case "success":
            alertTemplateLocation = "successAlert.html"
            break;
        case "info":
            alertTemplateLocation = "infoAlert.html"
            break;
        case "danger":
            alertTemplateLocation = "dangerAlert.html"
            break;
        default:
            alertTemplateLocation = "infoAlert.html"
            break;
    }

    $configParser = {
        templateLocation: alertTemplateLocation,
        data: {
            messagePrimary: messagePrimary,
            message: message
        },
        container: '#alert_placeholder'
    };
    parseTemplateWithConfigAppend($configParser);

    setTimeout(
        function () {
            $("#alertdiv").remove();
        },
        7500);
}