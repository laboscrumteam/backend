
$(document).ready(function () {
  vill = JSON.parse(sessionStorage.getItem("village"));
  loadMap(vill.locationX, vill.locationY);
});
var size = 5;
function testLoggedWithoutReloadAllContent() {

  var token = sessionStorage.getItem("acc_token");
  $.ajax({
    url: "api/testAuth",
    xhrFields: {
      withCredentials: true
    },
    headers: {
      'Authorization': `Bearer ` + token
    },
    method: 'GET',
    success: function (data) {
      loadBuildingData();
    },
    error: function () {
      showContent(content.NOT_LOGGER);
      showalert("Błąd logowania", "Zaloguj się ponownie na <a href=\"index.html\"> stronie głównej </a>. ", 'warning');
    }
  });
}
function loadMap(posX, posY) {
  var token = sessionStorage.getItem("acc_token");

  $.ajax({
    url: "api/map/" + posX + "/" + posY + "/" + size,
    xhrFields: {
      withCredentials: true
    },
    headers: {
      'Authorization': `Bearer ` + token
    },
    method: 'GET',
    success: function (data) {
      $("#inputGroup-locationX-input").val(posX);
      $("#inputGroup-locationY-input").val(posY);

      var locX;
      var locY;
      var iter;
      var xMin = posX - size, yMin = posY - size;
      for (iter = 0; iter < 2 * size; iter++) {
        $('#pos-x-' + iter).html(xMin + iter);
        $('#pos-y-' + iter).html(yMin + iter);
      }

      var villages = [];

      for (locX = 0; locX < 2 * size; locX++) {
        for (locY = 0; locY < 2 * size; locY++) {
          var c = data[locX][locY];
          if (c === null) {
            $('#c-' + locY + '-' + locX).html('<i class="fas fa-tree no-village"></i>');
          } else {
            lV = JSON.parse(sessionStorage.getItem("village"));

            villages.push(c);
            if (lV.locationX === c.locationX && lV.locationY === c.locationY) {
              $('#c-' + locY + '-' + locX).html('<i class="fab fa-fort-awesome my-village" onclick="showVillageModal(' + c.id + ')"></i>');
            } else {
              $('#c-' + locY + '-' + locX).html('<i class="fab fa-fort-awesome village" onclick="showVillageModal(' + c.id + ')"></i>');
            }
          }
        }
      }
      sessionStorage.setItem("map", JSON.stringify(villages));
    },
    error: function () {
      showContent(content.NOT_LOGGER);
      showalert("Błąd logowania", "Zaloguj się ponownie na <a href=\"index.html\"> stronie głównej </a>. ", 'warning');
    }
  });

}

function showVillageModal(id) {
  villages = JSON.parse(sessionStorage.getItem("map"));
  villages.forEach(function (entry) {
    if (entry.id === id) {
      $("#village-name-modal").html(entry.name);
      $("#village-owner-modal").html(entry.ownerName);
      $("#village-points-modal").html(entry.points);
      $("#village-locationX-modal").html(entry.locationX);
      $("#village-locationY-modal").html(entry.locationY);

      $("#village-modal").modal('show');
      return;
    }
  });

}
function goUp() {
  posX = $("#inputGroup-locationX-input").val();
  posY = $("#inputGroup-locationY-input").val();
  loadMap(posX, posY - size);
}
function goDown() {
  posX = $("#inputGroup-locationX-input").val();
  posY = $("#inputGroup-locationY-input").val();
  loadMap(posX, +posY + +size);
}
function goLeft() {
  posX = $("#inputGroup-locationX-input").val();
  posY = $("#inputGroup-locationY-input").val();
  loadMap(posX - size, posY);
}
function goRight() {
  posX = $("#inputGroup-locationX-input").val();
  posY = $("#inputGroup-locationY-input").val();
  loadMap(+posX + +size, posY);
}
function go() {
  posX = $("#inputGroup-locationX-input").val();
  posY = $("#inputGroup-locationY-input").val();
  loadMap(posX, posY);
}



