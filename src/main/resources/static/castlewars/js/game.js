var interval = null;

$(document).ready(function () {
  testLogged();
  startInterval();
});

const content = {
  NOT_LOGGER: 'not-logged',
  VILLAGE: 'village',
  CASTLE: 'castle',
  CENTRAL_SQUARE: "centralSquare",
  BARRACKS: "barracks",
  TIMBER_CAMP: "timberCamp",
  CLAY_PIT: "clayPit",
  IRON_MINE: "ironMine",
  WAREHOUSE: "warehouse",
  FARM: "farm",
  WALL: "wall"

};
function stopInterval() {
  clearInterval(interval);
}
function  startInterval() {
  interval = setInterval(intervalUpdate, 10000);
}

function intervalUpdate() {
  updateResources();
  loadCastle();
  loadBarracks();
  updateArmy();
}

function testLogged() {

  var token = sessionStorage.getItem("acc_token");
  $.ajax({
    url: "api/testAuth",
    xhrFields: {
      withCredentials: true
    },
    headers: {
      'Authorization': `Bearer ` + token
    },
    method: 'GET',
    success: function (data) {
      loadBuildingData();
      showContent(content.VILLAGE);
    },
    error: function () {
      showContent(content.NOT_LOGGER);
      showalert("Błąd logowania", "Zaloguj się ponownie na <a href=\"index.html\"> stronie głównej </a>. ", 'warning');
    }

  });
}
function testLoggedWithoutReloadAllContent() {

  var token = sessionStorage.getItem("acc_token");
  $.ajax({
    url: "api/testAuth",
    xhrFields: {
      withCredentials: true
    },
    headers: {
      'Authorization': `Bearer ` + token
    },
    method: 'GET',
    success: function (data) {
      loadBuildingData();
    },
    error: function () {
      showContent(content.NOT_LOGGER);
      showalert("Błąd logowania", "Zaloguj się ponownie na <a href=\"index.html\"> stronie głównej </a>. ", 'warning');
    }
  });
}

function updateResources() {
  var currentVillageId = sessionStorage.getItem("current_village");
  if (currentVillageId === '' || currentVillageId === null) {
    getVillages();
    return;
  }
  testLoggedWithoutReloadAllContent();
  var token = getAccessToken();
  $.ajax({
    url: "api/village/" + currentVillageId + "/resources",
    xhrFields: {
      withCredentials: true
    },
    headers: {
      'Authorization': `Bearer ` + token,
    },
    method: 'GET',
    success: function (data) {
      $resourceConfigParser = {
        templateLocation: "resources.html",
        data: data,
        container: '#resources'
      };
      parseTemplateWithConfig($resourceConfigParser);
    },
    error: function () {
      showalert("Błąd", "Spróbuj ponownie później.", 'warning');
    }

  });
}
function updateArmy() {
  var currentVillageId = sessionStorage.getItem("current_village");
  if (currentVillageId === '' || currentVillageId === null) {
    getVillages();
    return;
  }
  testLoggedWithoutReloadAllContent();
  var token = getAccessToken();
  $.ajax({
    url: "api/army/" + currentVillageId,
    xhrFields: {
      withCredentials: true
    },
    headers: {
      'Authorization': `Bearer ` + token
    },
    method: 'GET',
    success: function (data) {
      $resourceConfigParser = {
        templateLocation: "army.html",
        data: data,
        container: '#army'
      };
      parseTemplateWithConfig($resourceConfigParser);
    },
    error: function () {
      showalert("Błąd", "Spróbuj ponownie później.", 'warning');
    }

  });
}

function showContent(contentType) {
  switch (contentType) {
    case content.NOT_LOGGER:
      showContentById("#error-login");
      break;
    case content.VILLAGE:
      showContentById("#content-village");
      loadVillage();
      break;
    case content.CASTLE:
      showContentById("#castle");
      loadCastle();
      break;
    case content.CENTRAL_SQUARE:
      showContentById("#centralSquare");
      loadCentralSquare();
      break;
    case content.BARRACKS:
      showContentById("#barracks");
      loadBarracks();
      break;
    case content.TIMBER_CAMP:
      showContentById("#timberCamp");
      loadTimberCamp();
      break;
    case content.CLAY_PIT:
      showContentById("#clayPit");
      loadClayPit();
      break;
    case content.IRON_MINE:
      showContentById("#ironMine");
      loadIronMine();
      break;
    case content.WAREHOUSE:
      showContentById("#warehouse");
      loadWarehouse();
      break;
    case content.FARM:
      showContentById("#farm");
      loadFarm();
      break;
    case content.WALL:
      showContentById("#wall");
      loadWall();
      break;

  }
}
function loadCentralSquare() {

}

function loadBarracks() {
  var currentVillageId = sessionStorage.getItem("current_village");
  if (currentVillageId === '' || currentVillageId === null) {
    getVillages();
    return;
  }
  var token = getAccessToken();
  $.ajax({
    url: "api/army/" + currentVillageId + "/queue",
    xhrFields: {
      withCredentials: true
    },
    headers: {
      'Authorization': `Bearer ` + token
    },
    method: 'GET',
    success: function (data) {

      if (!jQuery.isEmptyObject(data)) {

        data.forEach(function (entry) {


          $village = JSON.parse(sessionStorage.getItem("village"));

          key = generateKey($village["barracks"], "BONUS");
          bonus = getBonus(key, "BARRACKS");

          time = getRecruitTime(entry.unitId);
          time = Math.round((time * bonus) / 100);
          lastUpdate = entry.lastUpdate;
          if (lastUpdate !== 0) {

            $now = new Date().getTime();
            $timeEnd = $now + time;
            //$currentTime = 

            $deltaNowStart = $now - lastUpdate;
            $deltaEndStart = $timeEnd - lastUpdate;
            $percentToEnd = Math.round(($deltaNowStart / $deltaEndStart) * 100);
            console.log($percentToEnd);
            entry.css = "" + $percentToEnd + "%";
          } else {
             entry.css = "0%";
          }

          entry.unitId = convertToUnitLocalName(entry.unitId);
        });

        $configParser2 = {
          templateLocation: "recruitQueue.html",
          data: data,
          container: '#recruitQueue'
        };
        parseTemplateWithConfig($configParser2);
      } else {
        $("#recruitQueue").empty();
      }




    },
    error: function () {
      showalert("Błąd", "Spróbuj ponownie później.", 'warning');
    }
  });


}
function loadTimberCamp() {

}
function loadClayPit() {

}
function loadIronMine() {

}
function loadWarehouse() {

}
function loadFarm() {

}
function loadWall() {

}

function loadCastle() {
  var currentVillageId = sessionStorage.getItem("current_village");
  if (currentVillageId === '' || currentVillageId === null) {
    getVillages();
    return;
  }
  var token = getAccessToken();
  $.ajax({
    url: "api/building/" + currentVillageId,
    xhrFields: {
      withCredentials: true
    },
    headers: {
      'Authorization': `Bearer ` + token
    },
    method: 'GET',
    success: function (data) {

      $inProgress = data.in_progress;
      if (!jQuery.isEmptyObject($inProgress)) {

        $inProgress.forEach(function (entry) {
          $endTimestamp = entry.endTimestamp;
          $startTimestamp = entry.startTimestamp;
          $now = new Date().getTime();
          $deltaNowStart = $now - $startTimestamp;
          $deltaEndStart = $endTimestamp - $startTimestamp;
          $percentToEnd = Math.round(($deltaNowStart / $deltaEndStart) * 100);
          console.log($deltaNowStart / $deltaEndStart);
          entry.css = "" + $percentToEnd + "%";
          //change time to epoch
          $timeText = new Date($endTimestamp).toLocaleString();

          entry.endTimestamp = $timeText;
          entry.buildingId = convertToBuildingLocalName(entry.buildingId);

        });

        console.log($inProgress);
        $configParser = {
          templateLocation: "buildingQueue.html",
          data: $inProgress,
          container: '#buildingsInProgress'
        };
        parseTemplateWithConfig($configParser);
      } else {
        $("#buildingsInProgress").empty();
      }

      $toReceive = data.ended;
      if (!jQuery.isEmptyObject($toReceive)) {


        $toReceive.forEach(function (entry) {
          entry.buildingId = convertToBuildingLocalName(entry.buildingId);
        });


        $configParser2 = {
          templateLocation: "buildingToReceive.html",
          data: $toReceive,
          container: '#buildingsToReceive'
        };
        parseTemplateWithConfig($configParser2);
      } else {
        $("#buildingsToReceive").empty();
      }

      loadNextLvlsFromServer();


    },
    error: function () {
      showalert("Błąd", "Spróbuj ponownie później.", 'warning');
    }
  });


}

function convertToBuildingLocalName(buildingId) {
  switch (buildingId) {
    case "HEADQUARTERS":
      return "Zamek";
    case "CENTRAL_SQUARE":
      return "Plac";
    case "BARRACKS":
      return "Koszary";
    case "TIMBER_CAMP":
      return "Tartak";
    case "CLAY_PIT":
      return "Cegielnia";
    case "IRON_MINE":
      return "Huta żelaza";
    case "WAREHOUSE":
      return "Spichlerz";
    case "FARM":
      return "Farma";
    case "WALL":
      return "Mur";
  }
}
function convertToUnitLocalName(unitId) {
  switch (unitId) {
    case "SPEAR_FIGHTER":
      return "Pikinier";
    case "SWORDSMAN":
      return "Miecznik";
    case "ARCHER":
      return "Łucznik";
    case "CAVALARY":
      return "Kawaleria";

  }
}
function getRecruitTime(unitId) {
  switch (unitId) {
    case "SPEAR_FIGHTER":
      return 175 * 1000;
    case "SWORDSMAN":
      return 230 * 1000;
    case "ARCHER":
      return 260 * 1000;
    case "CAVALARY":
      return 600 * 1000;
  }
}

function showContentById(elementID) {
  $(".content-element").each((index, element) => {
    $(element).addClass("d-none");
  });
  $(elementID).removeClass("d-none");
}

function receiveBuilding(event) {
  console.log("Receive building");
  $queueId = event.target.value;

  var currentVillageId = sessionStorage.getItem("current_village");
  if (currentVillageId === '' || currentVillageId === null) {
    getVillages();
    return;
  }

  var token = getAccessToken();
  $.ajax({
    url: "api/building/" + currentVillageId + "/received/" + $queueId,
    xhrFields: {
      withCredentials: true
    },
    headers: {
      'Authorization': `Bearer ` + token
    },
    method: 'GET',
    success: function (data) {
      setTimeout(loadCastle(), 300);
    },
    error: function () {
      showalert("Błąd", "Spróbuj ponownie później.", 'warning');
    }

  });
}

function loadVillage() {
  var currentVillageId = sessionStorage.getItem("current_village");
  if (currentVillageId === '' || currentVillageId === null) {
    getVillages();
    return;
  }
  var token = getAccessToken();
  $.ajax({
    url: "api/village/" + currentVillageId,
    xhrFields: {
      withCredentials: true
    },
    headers: {
      'Authorization': `Bearer ` + token,
    },
    method: 'GET',
    success: function (data) {
      console.log(data);
      // headquarters = data[];


      $.each(data, function (index, value) {
        console.log(index + " " + value);
        switch (index) {
          case "headquarters":
          case "barracks":
          case "wall":
          case "warehouse":
          case "farm":
            if (value > 0) {
              key = generateKey(value, "BONUS");
              bonus = getBonus(key, index.toUpperCase());
              //console.log(bonus);
              data[index + "_bonus"] = bonus;
            } else {
              data[index + "_bonus"] = "Nie wybudowano";
            }
            break;
          case "centralSquare":
            if (value > 0) {
              key = generateKey(value, "BONUS");
              bonus = getBonus(key, "CENTRAL_SQUARE");
              //console.log(bonus);
              data[index + "_bonus"] = bonus;
            } else {
              data[index + "_bonus"] = "Nie wybudowano";
            }
            break;
          case "ironMine":
            if (value > 0) {
              key = generateKey(value, "BONUS");
              bonus = getBonus(key, "IRON_MINE");
              //console.log(bonus);
              data[index + "_bonus"] = bonus;
            } else {
              data[index + "_bonus"] = "Nie wybudowano";
            }
            break;
          case "timberCamp":
            if (value > 0) {
              key = generateKey(value, "BONUS");
              bonus = getBonus(key, "TIMBER_CAMP");
              //console.log(bonus);
              data[index + "_bonus"] = bonus;
            } else {
              data[index + "_bonus"] = "Nie wybudowano";
            }
            break;
          case "clayPit":
            if (value > 0) {
              key = generateKey(value, "BONUS");
              bonus = getBonus(key, "CLAY_PIT");
              //console.log(bonus);
              data[index + "_bonus"] = bonus;
            } else {
              data[index + "_bonus"] = "Nie wybudowano";
            }
            break;

        }
      });
      $resourcess = {
        wood: data['wood'],
        iron: data['iron'],
        clay: data['clay'],
        population: data['population']
      };
      console.log(data);
      sessionStorage.setItem("village", JSON.stringify(data));
      $configParser = {
        templateLocation: "village-info.html",
        data: data,
        container: '#village-info'
      };

      parseTemplateWithConfig($configParser);
      $resourceConfigParser = {
        templateLocation: "resources.html",
        data: $resourcess,
        container: '#resources'
      };
      parseTemplateWithConfig($resourceConfigParser);
      hideLvl0Buildings();
      updateArmy();

    },
    error: function () {
      showalert("Błąd", "Spróbuj ponownie później.", 'warning');
    }

  });
}

function recruit() {
  unitt = $("#unitType").val();
  switch (unitt) {
    case "Pikinier":
      unitt = "SPEAR_FIGHTER";
      break;
    case "Miecznik":
      unitt = "SWORDSMAN";
      break;
    case "Łucznik":
      unitt = "ARCHER";
      break;
    case "Kawaleria":
      unitt = "CAVALARY";
      break;
  }
  toRecruit = $("#unitToRecruit").val();
  if (toRecruit < 1) {
    showalert("Niepoprawna liczba jednostek", "Podana liczba nie może być mniejsza od 1.", 'warning');
    return;
  }
  var currentVillageId = sessionStorage.getItem("current_village");
  if (currentVillageId === '' || currentVillageId === null) {
    getVillages();
    return;
  }

  var token = getAccessToken();
  $.ajax({
    url: "api/army/" + currentVillageId + "/recruit/" + unitt + "/" + toRecruit,
    xhrFields: {
      withCredentials: true
    },
    headers: {
      'Authorization': `Bearer ` + token
    },
    method: 'GET',
    success: function (data) {
      setTimeout(loadBarracks()(), 100);


      updateResources();
    },
    error: function (data) {
      console.log(data)
      if (data.responseText === 'Not enaugth resourcess or barracks lvl is to low.') {
        showalert("Niemożna rekrutować", "Niewystarczające zasoby albo koszary mają zbyt niski poziom.", 'info');
      } else if (data.responseText === 'Headquadrates must have a higher level') {
        showalert("UWAGA", "Zamek musi mieć wyższy poziom.", 'info');
      } else {
        showalert("Błąd", "Spróbuj ponownie później.", 'warning');
      }

    }
  });
}

function getVillages() {
  var token = getAccessToken();
  $.ajax({
    url: "api/village",
    xhrFields: {
      withCredentials: true
    },
    headers: {
      'Authorization': `Bearer ` + token,
    },
    method: 'GET',
    success: function (data) {
      if (data.length > 0) {
        console.log(data);
        var villageId = data[0];
        sessionStorage.setItem("current_village", villageId);
        loadVillage();
      } else {
        showalert("Przykro nam.", "Niestety nie posiadasz żadnej wioski na tym świecie.", 'warning');
      }
    },
    error: function () {
      showalert("Błąd", "Spróbuj ponownie później.", 'warning');
    }

  });
}

function loadNextLvlsFromServer() {

  var currentVillageId = sessionStorage.getItem("current_village");
  if (currentVillageId === '' || currentVillageId === null) {
    getVillages();
    return;
  }

  var token = getAccessToken();
  $.ajax({
    url: "api/building/" + currentVillageId + "/nextlvl",
    xhrFields: {
      withCredentials: true
    },
    headers: {
      'Authorization': `Bearer ` + token,
    },
    method: 'GET',
    success: function (data) {
      $preparedData = {};
      $.each(data, function (index, value) {
        switch (index) {
          case "CENTRAL_SQUARE":
            $preparedData.centralSquare = getNextLvlData("CENTRAL_SQUARE", value);
            break;
          case "WALL":
            $preparedData.wall = getNextLvlData("WALL", value);
            break;
          case "HEADQUARTERS":
            $preparedData.headquartes = getNextLvlData("HEADQUARTERS", value);
            break;
          case "IRON_MINE":
            $preparedData.ironMine = getNextLvlData("IRON_MINE", value);
            break;
          case "WAREHOUSE":
            $preparedData.warehouse = getNextLvlData("WAREHOUSE", value);
            break;
          case "FARM":
            $preparedData.farm = getNextLvlData("FARM", value);
            break;
          case "CLAY_PIT":
            $preparedData.clayPit = getNextLvlData("CLAY_PIT", value);
            break;
          case "BARRACKS":
            $preparedData.barracks = getNextLvlData("BARRACKS", value);
            break;
          case "TIMBER_CAMP":
            $preparedData.timberCamp = getNextLvlData("TIMBER_CAMP", value);
            break;
        }

      });
      console.log($preparedData);

      $configParser = {
        templateLocation: "buildingUpgradeList.html",
        data: $preparedData,
        container: '#buildingUpgradeList'
      };
      parseTemplateWithConfig($configParser);

    },
    error: function () {
      showalert("Błąd", "Spróbuj ponownie później.", 'warning');
    }

  });


}

function getNextLvlData(building, lvl) {
  $result = {};
  $isUndefinded = false;

  keyPeople = generateKey(lvl, "PEOPLE_COST");
  $peopleCost = getBonus(keyPeople, building);
  if ($peopleCost == undefined) {
    $peopleCost = '-';
    $isUndefinded = true;
  }
  $result.peopleCost = $peopleCost;

  keyIron = generateKey(lvl, "IRON_COST");
  $ironCost = getBonus(keyIron, building);
  if ($ironCost == undefined) {
    $ironCost = '-';
    $isUndefinded = true;
  }
  $result.ironCost = $ironCost;

  keyClay = generateKey(lvl, "CLAY_COST");
  $clayCost = getBonus(keyClay, building);
  if ($clayCost == undefined) {
    $clayCost = '-';
    $isUndefinded = true;
  }
  $result.clayCost = $clayCost;

  keyWood = generateKey(lvl, "WOOD_COST");
  $woodCost = getBonus(keyWood, building);
  if ($woodCost == undefined) {
    $woodCost = '-';
    $isUndefinded = true;
  }
  $result.woodCost = $woodCost;

  keyTime = generateKey(lvl, "BUILD_TIME");
  $buildTime = getBonus(keyTime, building);
  if ($buildTime == undefined) {
    $buildTime = '-';
    $isUndefinded = true;
  }
  var hours = Math.floor($buildTime / 60);
  var minutes = $buildTime % 60;
  $buildTime = hours + "h " + minutes + "m"
  $result.buildTime = $buildTime;

  if ($isUndefinded) {
    $result.css = 'd-none'
  } else {
    $result.css = '';
  }

  return $result;
}

function upgradeBuilding(building) {
  stopInterval();
  console.log(building + "upgrade")

  var currentVillageId = sessionStorage.getItem("current_village");
  if (currentVillageId === '' || currentVillageId === null) {
    getVillages();
    return;
  }

  var token = getAccessToken();
  $.ajax({
    url: "api/building/" + currentVillageId + "/" + building,
    xhrFields: {
      withCredentials: true
    },
    headers: {
      'Authorization': `Bearer ` + token
    },
    method: 'POST',
    success: function (data) {
      setTimeout(loadCastle(), 100);
      startInterval();
    },
    error: function (data) {
      console.log(data);
      if (data.responseText === 'Not enough resourcess') {
        showalert("UWAGA", "Niewystarczające zasoby.", 'info');
      } else if (data.responseText === 'Headquadrates must have a higher level') {
        showalert("UWAGA", "Zamek musi mieć wyższy poziom.", 'info');
      } else {
        showalert("Błąd", "Spróbuj ponownie później.", 'warning');
      }
      startInterval();
      updateResources();
    }
  });

}

function hideLvl0Buildings() {
  $(".building-list").each((index, element) => {
    lvlElement = $(element).find(".building-lvl");
    if (lvlElement.text() === "0") {
      $(element).addClass("d-none");
    }
  });
  ;
}

function getBonus(key, building) {
  var info = JSON.parse(localStorage.getItem("build_info"));
  //console.log(info);
  //console.log(key);
  //console.log(building);
  return info[building].properties[key];
}

function arrayRemove(arr, value) {

  return arr.filter(function (ele) {
    return ele !== value;
  });

}

function loadBuildingData() {
  var token = getAccessToken();
  $.ajax({
    url: "api/building/info",
    xhrFields: {
      withCredentials: true
    },
    headers: {
      'Authorization': `Bearer ` + token,
    },
    method: 'GET',
    success: function (data) {
      localStorage.setItem("build_info", JSON.stringify(data));
    },
    error: function () {
      console.error("Can not load buildings details");
    }

  });
}

function generateKey(valA, valB) {
  return "<" + valA + ", " + valB + ">"
}